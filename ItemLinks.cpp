/*
 * MacroQuest: The extension platform for EverQuest
 * Copyright (C) 2002-2021 MacroQuest Authors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "pch.h"
#include "ItemLinks.h"

#include "Globals.h"
#include "Items.h"

namespace eqlib {

FUNCTION_AT_VARIABLE_ADDRESS(void ConvertItemTags(CXStr&, bool), __ConvertItemTags);
FUNCTION_AT_VARIABLE_ADDRESS(CXStr CleanItemTags(const CXStr&, bool), __CleanItemTags);

// Used in ConvertItemTags, to be safe from change this could be imported from the game.
// This can be found from within ConvertItemTags
constexpr int TagSizes[ETAG_COUNT] = {
	93,  // 58 on emu
	3,
	3,
	0,
	0,
	0,
	0,
	0,
	0,
};

// Token used to signal the item tag in a text string
constexpr char ITEM_TAG_CHAR = '\x12';

// Looks for a link in the provided string. Returns the link if it exists. If no link is
// found, returns a link with ETAG_INVALID.
TextTagInfo ExtractLink(std::string_view inputString)
{
	TextTagInfo link;
	link.tagCode = ETAG_INVALID;

	// Need at least enough space for begin + end + code + contents
	if (inputString.length() < 4)
		return link;

	for (size_t i = 0; i < inputString.length(); ++i)
	{
		// look for starting character.
		char ch = inputString[i];
		if (ch == ITEM_TAG_CHAR)
		{
			// look for ending character.
			int end = -1;

			for (size_t pos = i + 2; pos < inputString.length(); ++pos)
			{
				if (inputString[pos] == ITEM_TAG_CHAR)
				{
					end = (int)pos + 1;
					break;
				}
			}

			if (end == -1)
			{
				// found starting tag but no ending tag.
				link.link = inputString.substr(i, 1);
				return link;
			}

			// link.link will hold the whole range of the link, including the \x12 characters.
			link.link = inputString.substr(i, end - i);
			end -= 1;

			link.tagCode = (ETagCodes)(inputString[i + 1] - '0');

			if (link.tagCode < ETAG_FIRST || link.tagCode > ETAG_LAST)
				return link;

			// the tag size determines where the text of the link is located relative to the
			// character after the tag code.
			const int tagSize = TagSizes[link.tagCode];
			size_t textStart = 0;

			switch (link.tagCode)
			{
			case ETAG_ITEM:
				textStart = i + (tagSize - 1);
				link.text = inputString.substr(textStart, end - textStart);
				return link;

			case ETAG_PLAYER:
				textStart = i + (tagSize - 1);
				link.text = inputString.substr(textStart, end - textStart);
				if (link.text.length() > 0 && link.text[0] == ':')
					link.text = link.text.substr(1);
				return link;

			case ETAG_SPAM:
				link.text = "(SPAM)";
				return link;

			case ETAG_ACHIEVEMENT:
			case ETAG_SPELL:
			case ETAG_FACTION:
				textStart = inputString.find('\'', i + 1);
				if (textStart != std::string_view::npos)
				{
					textStart += 1;
					link.text = inputString.substr(textStart, end - textStart);
				}
				return link;

			case ETAG_COMMAND2:
				textStart = inputString.find(':', i + 1);
				if (textStart != std::string_view::npos)
				{
					textStart += 1;
					link.text = inputString.substr(textStart, end - textStart);
					return link;
				}
				// fallthrough
			case ETAG_DIALOG_RESPONSE:
			case ETAG_COMMAND:
			default:
				textStart = i + tagSize + 2; // 2 skips the first marker and the tag code.
				link.text = inputString.substr(textStart, end - textStart);
				return link;
			}
		}
	}

	return link;
}

size_t ExtractLinks(std::string_view str, TextTagInfo* outTagInfo, size_t numTagInfos)
{
	std::string_view current = str;
	size_t count = 0;

	while (count < numTagInfos)
	{
		TextTagInfo& tagInfo = outTagInfo[count];
		tagInfo = ExtractLink(current);
		if (tagInfo.tagCode == ETAG_INVALID)
			break;

		count++;

		// Get offset of tag
		size_t pos = tagInfo.link.data() - current.data();
		if (pos < 0)
			break;

		current = current.substr(pos + tagInfo.link.length());
	}

	return count;
}

bool GetItemLink(ItemClient* pItem, char* Buffer, size_t BufferSize, bool Clickable)
{
	char hash[MAX_STRING] = { 0 };
	bool retVal = false;

	pItem->CreateItemTagString(hash, MAX_STRING, true);

	int len = strlen(hash);
	if (len > 0)
	{
		if (Clickable)
		{
			sprintf_s(Buffer, BufferSize, "%c%d%s%s%c", ITEM_TAG_CHAR, ETAG_ITEM, hash, pItem->GetName(), ITEM_TAG_CHAR);
		}
		else
		{
			sprintf_s(Buffer, BufferSize, "%d%s%s", ETAG_ITEM, hash, pItem->GetName());
		}

		retVal = true;
	}

	return retVal;
}

bool ParseItemLink(std::string_view link, ItemLinkInfo& linkInfo)
{
	// If we were given a full link, then break it down into just the data portion
	// (excluding the name).
	if ((int)link.length() > TagSizes[ETAG_ITEM] - 3)
	{
		if (link[0] == ITEM_TAG_CHAR)
		{
			TextTagInfo tagInfo = ExtractLink(link);
			if (tagInfo.tagCode != ETAG_ITEM)
				return false;

			std::string_view linkData = tagInfo.link.substr(2);
			link = linkData.substr(0, tagInfo.text.data() - linkData.data());

			linkInfo.itemName = tagInfo.text;
		}
		else
		{
			return false;
		}
	}

	// Validate size of link.
	if (link.length() != TagSizes[ETAG_ITEM] - 3)
		return false;

	const char* data = link.data();
	int ch = 0;

	ch += sscanf_s(data, "%5X", &linkInfo.itemID); data += 5;
	for (int i = 0; i < MAX_AUG_SOCKETS; ++i)
	{
		ch += sscanf_s(data, "%5X", &linkInfo.sockets[i]); data += 5;
		ch += sscanf_s(data, "%5X", &linkInfo.socketLuck[i]); data += 5;
	}

	int isEvolving = 0;
	ch += sscanf_s(data, "%1d", &isEvolving); data += 1;
	linkInfo.isEvolving = (isEvolving != 0);
	ch += sscanf_s(data, "%4X", &linkInfo.evolutionGroup); data += 4;
	ch += sscanf_s(data, "%2X", &linkInfo.evolutionLevel); data += 2;
	ch += sscanf_s(data, "%5X", &linkInfo.ornamentationIconID); data += 5;
	ch += sscanf_s(data, "%5X", &linkInfo.luck); data += 5;
	ch += sscanf_s(data, "%8X", &linkInfo.itemHash);

	// count number of items returned.
	return ch == (2 * MAX_AUG_SOCKETS) + 7;
}

} // namespace eqlib
