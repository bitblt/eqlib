/*
 * MacroQuest: The extension platform for EverQuest
 * Copyright (C) 2002-2021 MacroQuest Authors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "pch.h"
#include "EQLib.h"

#include "Common/StringUtils.h"

namespace eqlib {

// These don't change during the execution of the program. They can be loaded
// at static initialization time because of this.
uintptr_t EQGameBaseAddress = (uintptr_t)GetModuleHandle(nullptr);

uintptr_t EQGraphicsBaseAddress = (uintptr_t)GetModuleHandle("EQGraphicsDX9.dll");

uintptr_t EQMainBaseAddress = (uintptr_t)GetModuleHandle("eqmain.dll");

uintptr_t Kernel32BaseAddress = (uintptr_t)GetModuleHandle("kernel32.dll");

//============================================================================
// Data
//============================================================================

const char* szCombineTypes[] = {
#include "../eqdata/combines.h"
	nullptr
};
size_t MAX_COMBINES = lengthof(szCombineTypes);

const char* szItemClasses[] = {
#include "../eqdata/itemclasses.h"
	nullptr
};
size_t MAX_ITEMCLASSES = lengthof(szItemClasses);
const char** szItemTypes = szItemClasses;
size_t MAX_ITEMTYPES = lengthof(szItemClasses);

const char* szSPATypes[] = {
#include "../eqdata/spelleffects.h"
	nullptr
};
size_t MAX_SPELLEFFECTS = lengthof(szSPATypes) - 1;

const char* szFactionNames[] = {
#include "../eqdata/factionnames.h"
	nullptr
};
size_t MAX_FACTIONNAMES = lengthof(szFactionNames) - 1;

ACTORDEFENTRY ActorDefList[] = {
#include "../eqdata/actordef.h"
	0, 0, "NULL"
};

DIKEYID gDiKeyID[] = {
#include "../eqdata/dikeys.h"
	{ 0, 0 }
};

const char* gDiKeyName[256];

ServerID ServerIDArray[ServerID::NumServers] = {
	ServerID::Rizlona,
	ServerID::Lockjaw,
	ServerID::Ragefire,
	ServerID::Vox,
	ServerID::Trakanon,
	ServerID::Fippy,
	ServerID::Vulak,
	ServerID::Mayong,
	ServerID::Antonius,
	ServerID::Brekt,
	ServerID::Bertox,
	ServerID::Bristle,
	ServerID::Cazic,
	ServerID::Drinal,
	ServerID::Erollisi,
	ServerID::Firiona,
	ServerID::Luclin,
	ServerID::Povar,
	ServerID::Rathe,
	ServerID::Tunare,
	ServerID::Xegony,
	ServerID::Zek,
};

const char* GetServerNameFromServerID(ServerID id)
{
	switch (id)
	{
	case ServerID::Rizlona: return "rizlona";
	case ServerID::Lockjaw: return "lockjaw";
	case ServerID::Ragefire: return "ragefire";
	case ServerID::Vox: return "vox";
	case ServerID::Trakanon: return "trakanon";
	case ServerID::Fippy: return "fippy";
	case ServerID::Vulak: return "vulak";
	case ServerID::Mayong: return "mayong";
	case ServerID::Antonius: return "antonius";
	case ServerID::Brekt: return "brekt";
	case ServerID::Bertox: return "bertox";
	case ServerID::Bristle: return "bristle";
	case ServerID::Cazic: return "cazic";
	case ServerID::Drinal: return "drinal";
	case ServerID::Erollisi: return "erollisi";
	case ServerID::Firiona: return "firiona";
	case ServerID::Luclin: return "luclin";
	case ServerID::Povar: return "povar";
	case ServerID::Rathe: return "rathe";
	case ServerID::Tunare: return "tunare";
	case ServerID::Xegony: return "xegony";
	case ServerID::Zek: return "zek";
	}

	return "unknown";
}

ServerID GetServerIDFromServerName(const char* serverName)
{
	static const mq::ci_unordered::map<std::string_view, ServerID> serverMapping{
		{ "rizlona", ServerID::Rizlona },
		{ "lockjaw", ServerID::Lockjaw },
		{ "ragefire", ServerID::Ragefire },
		{ "vox", ServerID::Vox },
		{ "trakanon", ServerID::Trakanon },
		{ "fippy", ServerID::Fippy },
		{ "vulak", ServerID::Vulak },
		{ "mayong", ServerID::Mayong },
		{ "antonius", ServerID::Antonius },
		{ "brekt", ServerID::Brekt },
		{ "bertox", ServerID::Bertox },
		{ "bristle", ServerID::Bristle },
		{ "cazic", ServerID::Cazic },
		{ "drinal", ServerID::Drinal },
		{ "erollisi", ServerID::Erollisi },
		{ "firiona", ServerID::Firiona },
		{ "luclin", ServerID::Luclin },
		{ "povar", ServerID::Povar },
		{ "rathe", ServerID::Rathe },
		{ "tunare", ServerID::Tunare },
		{ "xegony", ServerID::Xegony },
		{ "zek", ServerID::Zek },
	};

	auto iter = serverMapping.find(serverName);
	if (iter != serverMapping.end())
		return iter->second;

	return ServerID::Invalid;
}


//============================================================================
// Offset Definitions & Initialization
//============================================================================

#pragma region eqgame.exe offsets
//============================================================================
//
// eqgame.exe Offsets

INITIALIZE_EQGAME_OFFSET(__ActualVersionBuild);
INITIALIZE_EQGAME_OFFSET(__ActualVersionDate);
INITIALIZE_EQGAME_OFFSET(__ActualVersionTime);
INITIALIZE_EQGAME_OFFSET(__Attack);
INITIALIZE_EQGAME_OFFSET(__Autofire);
INITIALIZE_EQGAME_OFFSET(__AutoSkillArray);
INITIALIZE_EQGAME_OFFSET(__BindList);
INITIALIZE_EQGAME_OFFSET(__Clicks);
INITIALIZE_EQGAME_OFFSET(__CommandList);
INITIALIZE_EQGAME_OFFSET(__CurrentMapLabel);
INITIALIZE_EQGAME_OFFSET(__CurrentSocial);
INITIALIZE_EQGAME_OFFSET(__do_loot);
INITIALIZE_EQGAME_OFFSET(__DoAbilityList);
INITIALIZE_EQGAME_OFFSET(__gpbCommandEvent);
INITIALIZE_EQGAME_OFFSET(__GroupAggro);
INITIALIZE_EQGAME_OFFSET(__Guilds);
INITIALIZE_EQGAME_OFFSET(__gWorld);
INITIALIZE_EQGAME_OFFSET(__heqmain);
INITIALIZE_EQGAME_OFFSET(__HWnd);
INITIALIZE_EQGAME_OFFSET(__InChatMode);
INITIALIZE_EQGAME_OFFSET(__Inviter);
INITIALIZE_EQGAME_OFFSET(__LastTell);
INITIALIZE_EQGAME_OFFSET(__LMouseHeldTime);
INITIALIZE_EQGAME_OFFSET(__LoginName);
INITIALIZE_EQGAME_OFFSET(__Mouse);
INITIALIZE_EQGAME_OFFSET(__MouseEventTime);
INITIALIZE_EQGAME_OFFSET(__MouseLook);
INITIALIZE_EQGAME_OFFSET(__NetStatusToggle);
INITIALIZE_EQGAME_OFFSET(__PCNames);
INITIALIZE_EQGAME_OFFSET(__RangeAttackReady);
INITIALIZE_EQGAME_OFFSET(__RMouseHeldTime);
INITIALIZE_EQGAME_OFFSET(__RunWalkState);
INITIALIZE_EQGAME_OFFSET(__ScreenMode);
INITIALIZE_EQGAME_OFFSET(__ScreenX);
INITIALIZE_EQGAME_OFFSET(__ScreenXMax);
INITIALIZE_EQGAME_OFFSET(__ScreenY);
INITIALIZE_EQGAME_OFFSET(__ScreenYMax);
INITIALIZE_EQGAME_OFFSET(__ServerHost);
INITIALIZE_EQGAME_OFFSET(__ServerName);
INITIALIZE_EQGAME_OFFSET(__ShiftKeyDown);
INITIALIZE_EQGAME_OFFSET(__ShowNames);
INITIALIZE_EQGAME_OFFSET(__SocialChangedList);
INITIALIZE_EQGAME_OFFSET(__Socials);
INITIALIZE_EQGAME_OFFSET(__SubscriptionType);
INITIALIZE_EQGAME_OFFSET(__TargetAggroHolder);
INITIALIZE_EQGAME_OFFSET(__ThrottleFrameRate);
INITIALIZE_EQGAME_OFFSET(__ThrottleFrameRateEnd);
INITIALIZE_EQGAME_OFFSET(__UseTellWindows);
INITIALIZE_EQGAME_OFFSET(__ZoneType);
INITIALIZE_EQGAME_OFFSET(EQObject_Top);
INITIALIZE_EQGAME_OFFSET(g_eqCommandStates);
INITIALIZE_EQGAME_OFFSET(instCRaid);
INITIALIZE_EQGAME_OFFSET(instDynamicZone);
INITIALIZE_EQGAME_OFFSET(instEQMisc);
INITIALIZE_EQGAME_OFFSET(instEQZoneInfo);
INITIALIZE_EQGAME_OFFSET(instExpeditionLeader);
INITIALIZE_EQGAME_OFFSET(instExpeditionName);
INITIALIZE_EQGAME_OFFSET(instTributeActive);
INITIALIZE_EQGAME_OFFSET(pinstActiveBanker);
INITIALIZE_EQGAME_OFFSET(pinstActiveCorpse);
INITIALIZE_EQGAME_OFFSET(pinstActiveGMaster);
INITIALIZE_EQGAME_OFFSET(pinstActiveMerchant);
INITIALIZE_EQGAME_OFFSET(pinstAltAdvManager);
INITIALIZE_EQGAME_OFFSET(pinstBandageTarget);
INITIALIZE_EQGAME_OFFSET(pinstCAAWnd);
INITIALIZE_EQGAME_OFFSET(pinstCAchievementsWnd);
INITIALIZE_EQGAME_OFFSET(pinstCActionsWnd);
INITIALIZE_EQGAME_OFFSET(pinstCAdvancedLootWnd);
INITIALIZE_EQGAME_OFFSET(pinstCAdventureLeaderboardWnd);
INITIALIZE_EQGAME_OFFSET(pinstCAdventureRequestWnd);
INITIALIZE_EQGAME_OFFSET(pinstCAdventureStatsWnd);
INITIALIZE_EQGAME_OFFSET(pinstCAlarmWnd);
INITIALIZE_EQGAME_OFFSET(pinstCAltStorageWnd);
INITIALIZE_EQGAME_OFFSET(pinstCamActor);
INITIALIZE_EQGAME_OFFSET(pinstCAudioTriggersWindow);
INITIALIZE_EQGAME_OFFSET(pinstCAuraWnd);
INITIALIZE_EQGAME_OFFSET(pinstCAvaZoneWnd);
INITIALIZE_EQGAME_OFFSET(pinstCBandolierWnd);
INITIALIZE_EQGAME_OFFSET(pinstCBankWnd);
INITIALIZE_EQGAME_OFFSET(pinstCBarterMerchantWnd);
INITIALIZE_EQGAME_OFFSET(pinstCBarterSearchWnd);
INITIALIZE_EQGAME_OFFSET(pinstCBarterWnd);
INITIALIZE_EQGAME_OFFSET(pinstCBazaarSearchWnd);
INITIALIZE_EQGAME_OFFSET(pinstCBazaarWnd);
INITIALIZE_EQGAME_OFFSET(pinstCBlockedBuffWnd);
INITIALIZE_EQGAME_OFFSET(pinstCBlockedPetBuffWnd);
INITIALIZE_EQGAME_OFFSET(pinstCBodyTintWnd);
INITIALIZE_EQGAME_OFFSET(pinstCBookWnd);
INITIALIZE_EQGAME_OFFSET(pinstCBreathWnd);
INITIALIZE_EQGAME_OFFSET(pinstCBuffWindowNORMAL);
INITIALIZE_EQGAME_OFFSET(pinstCBuffWindowSHORT);
INITIALIZE_EQGAME_OFFSET(pinstCBugReportWnd);
INITIALIZE_EQGAME_OFFSET(pinstCCastingWnd);
INITIALIZE_EQGAME_OFFSET(pinstCCastSpellWnd);
INITIALIZE_EQGAME_OFFSET(pinstCCharacterListWnd);
INITIALIZE_EQGAME_OFFSET(pinstCChatWindowManager);
INITIALIZE_EQGAME_OFFSET(pinstCColorPickerWnd);
INITIALIZE_EQGAME_OFFSET(pinstCCombatAbilityWnd);
INITIALIZE_EQGAME_OFFSET(pinstCCombatSkillsSelectWnd);
INITIALIZE_EQGAME_OFFSET(pinstCCompassWnd);
INITIALIZE_EQGAME_OFFSET(pinstCConfirmationDialog);
INITIALIZE_EQGAME_OFFSET(pinstCContainerMgr);
INITIALIZE_EQGAME_OFFSET(pinstCContextMenuManager);
INITIALIZE_EQGAME_OFFSET(pinstCCursorAttachment);
INITIALIZE_EQGAME_OFFSET(pinstCDBStr);
INITIALIZE_EQGAME_OFFSET(pinstCDisplay);
INITIALIZE_EQGAME_OFFSET(pinstCDynamicZoneWnd);
INITIALIZE_EQGAME_OFFSET(pinstCEditLabelWnd);
INITIALIZE_EQGAME_OFFSET(pinstCEQMainWnd);
INITIALIZE_EQGAME_OFFSET(pinstCEverQuest);
INITIALIZE_EQGAME_OFFSET(pinstCExtendedTargetWnd);
INITIALIZE_EQGAME_OFFSET(pinstCFactionWnd);
INITIALIZE_EQGAME_OFFSET(pinstCFellowshipWnd);
INITIALIZE_EQGAME_OFFSET(pinstCFileSelectionWnd);
INITIALIZE_EQGAME_OFFSET(pinstCFindItemWnd);
INITIALIZE_EQGAME_OFFSET(pinstCFindLocationWnd);
INITIALIZE_EQGAME_OFFSET(pinstCFriendsWnd);
INITIALIZE_EQGAME_OFFSET(pinstCGemsGameWnd);
INITIALIZE_EQGAME_OFFSET(pinstCGiveWnd);
INITIALIZE_EQGAME_OFFSET(pinstCGroupSearchFiltersWnd);
INITIALIZE_EQGAME_OFFSET(pinstCGroupSearchWnd);
INITIALIZE_EQGAME_OFFSET(pinstCGroupWnd);
INITIALIZE_EQGAME_OFFSET(pinstCGuildBankWnd);
INITIALIZE_EQGAME_OFFSET(pinstCGuildMgmtWnd);
INITIALIZE_EQGAME_OFFSET(pinstCharacterCreation);
INITIALIZE_EQGAME_OFFSET(pinstCHotButtonWnd);
INITIALIZE_EQGAME_OFFSET(pinstCHotButtonWnd1);
INITIALIZE_EQGAME_OFFSET(pinstCHotButtonWnd2);
INITIALIZE_EQGAME_OFFSET(pinstCHotButtonWnd3);
INITIALIZE_EQGAME_OFFSET(pinstCHotButtonWnd4);
INITIALIZE_EQGAME_OFFSET(pinstCInspectWnd);
INITIALIZE_EQGAME_OFFSET(pinstCInventoryWnd);
INITIALIZE_EQGAME_OFFSET(pinstCInvSlotMgr);
INITIALIZE_EQGAME_OFFSET(pinstCItemDisplayManager);
INITIALIZE_EQGAME_OFFSET(pinstCItemExpTransferWnd);
INITIALIZE_EQGAME_OFFSET(pinstCJournalCatWnd);
INITIALIZE_EQGAME_OFFSET(pinstCJournalTextWnd);
INITIALIZE_EQGAME_OFFSET(pinstCKeyRingWnd);
INITIALIZE_EQGAME_OFFSET(pinstCLargeDialogWnd);
INITIALIZE_EQGAME_OFFSET(pinstCLFGuildWnd);
INITIALIZE_EQGAME_OFFSET(pinstCLoadskinWnd);
INITIALIZE_EQGAME_OFFSET(pinstCLootWnd);
INITIALIZE_EQGAME_OFFSET(pinstCMailAddressBookWnd);
INITIALIZE_EQGAME_OFFSET(pinstCMailCompositionWnd);
INITIALIZE_EQGAME_OFFSET(pinstCMailWnd);
INITIALIZE_EQGAME_OFFSET(pinstCMapToolbarWnd);
INITIALIZE_EQGAME_OFFSET(pinstCMapViewWnd);
INITIALIZE_EQGAME_OFFSET(pinstCMarketplaceWnd);
INITIALIZE_EQGAME_OFFSET(pinstCMerchantWnd);
INITIALIZE_EQGAME_OFFSET(pinstCMIZoneSelectWnd);
INITIALIZE_EQGAME_OFFSET(pinstCMusicPlayerWnd);
INITIALIZE_EQGAME_OFFSET(pinstCNoteWnd);
INITIALIZE_EQGAME_OFFSET(pinstControlledMissile);
INITIALIZE_EQGAME_OFFSET(pinstControlledPlayer);
INITIALIZE_EQGAME_OFFSET(pinstCOptionsWnd);
INITIALIZE_EQGAME_OFFSET(pinstCOverseerWnd);
INITIALIZE_EQGAME_OFFSET(pinstCPetInfoWnd);
INITIALIZE_EQGAME_OFFSET(pinstCPetitionQWnd);
INITIALIZE_EQGAME_OFFSET(pinstCPlayerCustomizationWnd);
INITIALIZE_EQGAME_OFFSET(pinstCPlayerNotesWnd);
INITIALIZE_EQGAME_OFFSET(pinstCPlayerWnd);
INITIALIZE_EQGAME_OFFSET(pinstCPopupWndManager);
INITIALIZE_EQGAME_OFFSET(pinstCProgressionSelectionWnd);
INITIALIZE_EQGAME_OFFSET(pinstCPurchaseGroupWnd);
INITIALIZE_EQGAME_OFFSET(pinstCPvPStatsWnd);
INITIALIZE_EQGAME_OFFSET(pinstCQuantityWnd);
INITIALIZE_EQGAME_OFFSET(pinstCRaidOptionsWnd);
INITIALIZE_EQGAME_OFFSET(pinstCRaidWnd);
INITIALIZE_EQGAME_OFFSET(pinstCRealEstateItemsWnd);
INITIALIZE_EQGAME_OFFSET(pinstCResolutionHandler);
INITIALIZE_EQGAME_OFFSET(pinstCRespawnWnd);
INITIALIZE_EQGAME_OFFSET(pinstCSelectorWnd);
INITIALIZE_EQGAME_OFFSET(pinstCServerListWnd);
INITIALIZE_EQGAME_OFFSET(pinstCSidlManager);
INITIALIZE_EQGAME_OFFSET(pinstCSkillsSelectWnd);
INITIALIZE_EQGAME_OFFSET(pinstCSkillsWnd);
INITIALIZE_EQGAME_OFFSET(pinstCSocialEditWnd);
INITIALIZE_EQGAME_OFFSET(pinstCSpellBookWnd);
INITIALIZE_EQGAME_OFFSET(pinstCStoryWnd);
INITIALIZE_EQGAME_OFFSET(pinstCTargetWnd);
INITIALIZE_EQGAME_OFFSET(pinstCTaskManager);
INITIALIZE_EQGAME_OFFSET(pinstCTaskTemplateSelectWnd);
INITIALIZE_EQGAME_OFFSET(pinstCTaskWnd);
INITIALIZE_EQGAME_OFFSET(pinstCTextEntryWnd);
INITIALIZE_EQGAME_OFFSET(pinstCTimeLeftWnd);
INITIALIZE_EQGAME_OFFSET(pinstCTipWndCONTEXT);
INITIALIZE_EQGAME_OFFSET(pinstCTipWndOFDAY);
INITIALIZE_EQGAME_OFFSET(pinstCTitleWnd);
INITIALIZE_EQGAME_OFFSET(pinstCTrackingWnd);
INITIALIZE_EQGAME_OFFSET(pinstCTradeWnd);
INITIALIZE_EQGAME_OFFSET(pinstCTrainWnd);
INITIALIZE_EQGAME_OFFSET(pinstCVideoModesWnd);
INITIALIZE_EQGAME_OFFSET(pinstCVoiceMacroWnd);
INITIALIZE_EQGAME_OFFSET(pinstCXWndManager);
INITIALIZE_EQGAME_OFFSET(pinstCZoneGuideWnd);
INITIALIZE_EQGAME_OFFSET(pinstDZMember);
INITIALIZE_EQGAME_OFFSET(pinstDZTimerInfo);
INITIALIZE_EQGAME_OFFSET(pinstEqLogin);
INITIALIZE_EQGAME_OFFSET(pinstEQSoundManager);
INITIALIZE_EQGAME_OFFSET(pinstEQSpellStrings);
INITIALIZE_EQGAME_OFFSET(pinstEQSuiteTextureLoader);
INITIALIZE_EQGAME_OFFSET(pinstEverQuestInfo);
INITIALIZE_EQGAME_OFFSET(pinstGroup);
INITIALIZE_EQGAME_OFFSET(pinstImeManager);
INITIALIZE_EQGAME_OFFSET(pinstItemIconCache);
INITIALIZE_EQGAME_OFFSET(pinstLocalPC);
INITIALIZE_EQGAME_OFFSET(pinstLocalPlayer);
INITIALIZE_EQGAME_OFFSET(pinstMercenaryData);
INITIALIZE_EQGAME_OFFSET(pinstMercenaryStats);
INITIALIZE_EQGAME_OFFSET(pinstModelPlayer);
INITIALIZE_EQGAME_OFFSET(pinstRenderInterface);
INITIALIZE_EQGAME_OFFSET(pinstPlayerPath);
INITIALIZE_EQGAME_OFFSET(pinstRewardSelectionWnd);
INITIALIZE_EQGAME_OFFSET(pinstSGraphicsEngine);
INITIALIZE_EQGAME_OFFSET(pinstSkillMgr);
INITIALIZE_EQGAME_OFFSET(pinstSpawnManager);
INITIALIZE_EQGAME_OFFSET(pinstSpellManager);
INITIALIZE_EQGAME_OFFSET(pinstSpellSets);
INITIALIZE_EQGAME_OFFSET(pinstStringTable);
INITIALIZE_EQGAME_OFFSET(pinstSwitchManager);
INITIALIZE_EQGAME_OFFSET(pinstTarget);
INITIALIZE_EQGAME_OFFSET(pinstTargetIndicator);
INITIALIZE_EQGAME_OFFSET(pinstTargetObject);
INITIALIZE_EQGAME_OFFSET(pinstTargetSwitch);
INITIALIZE_EQGAME_OFFSET(pinstTaskMember);
INITIALIZE_EQGAME_OFFSET(pinstTrackTarget);
INITIALIZE_EQGAME_OFFSET(pinstTradeTarget);
INITIALIZE_EQGAME_OFFSET(pinstViewActor);
INITIALIZE_EQGAME_OFFSET(pinstWorldData);

INITIALIZE_EQGAME_OFFSET(__MemChecker0);
INITIALIZE_EQGAME_OFFSET(__MemChecker1);
INITIALIZE_EQGAME_OFFSET(__MemChecker2);
INITIALIZE_EQGAME_OFFSET(__MemChecker3);
INITIALIZE_EQGAME_OFFSET(__MemChecker4);
INITIALIZE_EQGAME_OFFSET(__EncryptPad0);
INITIALIZE_EQGAME_OFFSET(DI8__Main);
INITIALIZE_EQGAME_OFFSET(DI8__Keyboard);
INITIALIZE_EQGAME_OFFSET(DI8__Mouse);
INITIALIZE_EQGAME_OFFSET(DI8__Mouse_Copy);
INITIALIZE_EQGAME_OFFSET(DI8__Mouse_Check);

INITIALIZE_EQGAME_OFFSET(__CastRay);
INITIALIZE_EQGAME_OFFSET(__CastRay2);
INITIALIZE_EQGAME_OFFSET(__CleanItemTags);
INITIALIZE_EQGAME_OFFSET(__ConvertItemTags);
INITIALIZE_EQGAME_OFFSET(__CopyLayout);
INITIALIZE_EQGAME_OFFSET(__CreateCascadeMenuItems);
INITIALIZE_EQGAME_OFFSET(__DoesFileExist);
INITIALIZE_EQGAME_OFFSET(__eq_delete);
INITIALIZE_EQGAME_OFFSET(__eq_new);
INITIALIZE_EQGAME_OFFSET(__EQGetTime);
INITIALIZE_EQGAME_OFFSET(__ExecuteCmd);
INITIALIZE_EQGAME_OFFSET(__FixHeading);
INITIALIZE_EQGAME_OFFSET(__FlushDxKeyboard);
INITIALIZE_EQGAME_OFFSET(__GameLoop);
INITIALIZE_EQGAME_OFFSET(__get_bearing);
INITIALIZE_EQGAME_OFFSET(__get_melee_range);
INITIALIZE_EQGAME_OFFSET(__GetAnimationCache);
INITIALIZE_EQGAME_OFFSET(__GetGaugeValueFromEQ);
INITIALIZE_EQGAME_OFFSET(__GetLabelFromEQ);
INITIALIZE_EQGAME_OFFSET(__GetXTargetType);
INITIALIZE_EQGAME_OFFSET(__HandleMouseWheel);
INITIALIZE_EQGAME_OFFSET(__HeadingDiff);
INITIALIZE_EQGAME_OFFSET(__HelpPath);
INITIALIZE_EQGAME_OFFSET(__IsResEffectSpell);
INITIALIZE_EQGAME_OFFSET(__ExecuteFrontEnd);
INITIALIZE_EQGAME_OFFSET(__msgTokenTextParam);
INITIALIZE_EQGAME_OFFSET(__NewUIINI);
INITIALIZE_EQGAME_OFFSET(__ProcessGameEvents);
INITIALIZE_EQGAME_OFFSET(__ProcessKeyboardEvents);
INITIALIZE_EQGAME_OFFSET(__ProcessMouseEvents);
INITIALIZE_EQGAME_OFFSET(__SaveColors);
INITIALIZE_EQGAME_OFFSET(__STMLToText);
INITIALIZE_EQGAME_OFFSET(__WndProc);

INITIALIZE_EQGAME_OFFSET(AggroMeterManagerClient__Instance);
INITIALIZE_EQGAME_OFFSET(AltAdvManager__CanSeeAbility);
INITIALIZE_EQGAME_OFFSET(AltAdvManager__CanTrainAbility);
INITIALIZE_EQGAME_OFFSET(AltAdvManager__GetAAById);
INITIALIZE_EQGAME_OFFSET(AltAdvManager__GetCalculatedTimer);
INITIALIZE_EQGAME_OFFSET(AltAdvManager__IsAbilityReady);
INITIALIZE_EQGAME_OFFSET(ArrayClass__DeleteElement);
INITIALIZE_EQGAME_OFFSET(BaseProfile__GetItemPossession);
INITIALIZE_EQGAME_OFFSET(CAAWnd__ShowAbility);
INITIALIZE_EQGAME_OFFSET(CAAWnd__Update);
INITIALIZE_EQGAME_OFFSET(CAAWnd__UpdateSelected);
INITIALIZE_EQGAME_OFFSET(CAdvancedLootWnd__AddPlayerToList);
INITIALIZE_EQGAME_OFFSET(CAdvancedLootWnd__CAdvancedLootWnd);
INITIALIZE_EQGAME_OFFSET(CAdvancedLootWnd__DoAdvLootAction);
INITIALIZE_EQGAME_OFFSET(CAdvancedLootWnd__DoSharedAdvLootAction);
INITIALIZE_EQGAME_OFFSET(CAdvancedLootWnd__UpdateMasterLooter);
INITIALIZE_EQGAME_OFFSET(CAltAbilityData__GetMaxRank);
INITIALIZE_EQGAME_OFFSET(CAltAbilityData__GetMercCurrentRank);
INITIALIZE_EQGAME_OFFSET(CAltAbilityData__GetMercMaxRank);
INITIALIZE_EQGAME_OFFSET(CBankWnd__GetNumBankSlots);
INITIALIZE_EQGAME_OFFSET(CBankWnd__WndNotification);
INITIALIZE_EQGAME_OFFSET(CBarterWnd__WndNotification);
INITIALIZE_EQGAME_OFFSET(CBarterSearchWnd__WndNotification);
INITIALIZE_EQGAME_OFFSET(CBarterSearchWnd__UpdateInventoryList);
INITIALIZE_EQGAME_OFFSET(CBazaarSearchWnd__HandleSearchResults);
INITIALIZE_EQGAME_OFFSET(CBroadcast__Get);
INITIALIZE_EQGAME_OFFSET(CButtonWnd__CButtonWnd);
INITIALIZE_EQGAME_OFFSET(CButtonWnd__dCButtonWnd);
INITIALIZE_EQGAME_OFFSET(CButtonWnd__vftable);
INITIALIZE_EQGAME_OFFSET(CCastSpellWnd__ForgetMemorizedSpell);
INITIALIZE_EQGAME_OFFSET(CCastSpellWnd__IsBardSongPlaying);
INITIALIZE_EQGAME_OFFSET(CCastSpellWnd__RefreshSpellGemButtons);
INITIALIZE_EQGAME_OFFSET(CCharacterListWnd__EnterWorld);
INITIALIZE_EQGAME_OFFSET(CCharacterListWnd__Quit);
INITIALIZE_EQGAME_OFFSET(CCharacterListWnd__SelectCharacter);
INITIALIZE_EQGAME_OFFSET(CCharacterListWnd__UpdateList);
INITIALIZE_EQGAME_OFFSET(CChatService__GetFriendName);
INITIALIZE_EQGAME_OFFSET(CChatService__GetNumberOfFriends);
INITIALIZE_EQGAME_OFFSET(CChatWindow__AddHistory);
INITIALIZE_EQGAME_OFFSET(CChatWindow__CChatWindow);
INITIALIZE_EQGAME_OFFSET(CChatWindow__Clear);
INITIALIZE_EQGAME_OFFSET(CChatWindow__WndNotification);
INITIALIZE_EQGAME_OFFSET(CChatWindowManager__CreateChatWindow);
INITIALIZE_EQGAME_OFFSET(CChatWindowManager__FreeChatWindow);
INITIALIZE_EQGAME_OFFSET(CChatWindowManager__GetRGBAFromIndex);
INITIALIZE_EQGAME_OFFSET(CChatWindowManager__InitContextMenu);
INITIALIZE_EQGAME_OFFSET(CChatWindowManager__SetLockedActiveChatWindow);
INITIALIZE_EQGAME_OFFSET(CColorPickerWnd__Open);
INITIALIZE_EQGAME_OFFSET(CCombatSkillsSelectWnd__ShouldDisplayThisSkill);
INITIALIZE_EQGAME_OFFSET(CComboWnd__DeleteAll);
INITIALIZE_EQGAME_OFFSET(CComboWnd__Draw);
INITIALIZE_EQGAME_OFFSET(CComboWnd__GetChoiceText);
INITIALIZE_EQGAME_OFFSET(CComboWnd__GetCurChoice);
INITIALIZE_EQGAME_OFFSET(CComboWnd__GetCurChoiceText);
INITIALIZE_EQGAME_OFFSET(CComboWnd__GetItemCount);
INITIALIZE_EQGAME_OFFSET(CComboWnd__GetListRect);
INITIALIZE_EQGAME_OFFSET(CComboWnd__GetTextRect);
INITIALIZE_EQGAME_OFFSET(CComboWnd__InsertChoice);
INITIALIZE_EQGAME_OFFSET(CComboWnd__InsertChoiceAtIndex);
INITIALIZE_EQGAME_OFFSET(CComboWnd__SetChoice);
INITIALIZE_EQGAME_OFFSET(CComboWnd__SetColors);
INITIALIZE_EQGAME_OFFSET(CContainerMgr__CloseContainer);
INITIALIZE_EQGAME_OFFSET(CContainerMgr__OpenContainer);
INITIALIZE_EQGAME_OFFSET(CContainerMgr__OpenExperimentContainer);
INITIALIZE_EQGAME_OFFSET(CContainerWnd__HandleCombine);
INITIALIZE_EQGAME_OFFSET(CContainerWnd__SetContainer);
INITIALIZE_EQGAME_OFFSET(CContainerWnd__vftable);
INITIALIZE_EQGAME_OFFSET(CContextMenu__AddMenuItem);
INITIALIZE_EQGAME_OFFSET(CContextMenu__AddSeparator);
INITIALIZE_EQGAME_OFFSET(CContextMenu__CContextMenu);
INITIALIZE_EQGAME_OFFSET(CContextMenu__CheckMenuItem);
INITIALIZE_EQGAME_OFFSET(CContextMenu__dCContextMenu);
INITIALIZE_EQGAME_OFFSET(CContextMenu__RemoveAllMenuItems);
INITIALIZE_EQGAME_OFFSET(CContextMenu__RemoveMenuItem);
INITIALIZE_EQGAME_OFFSET(CContextMenu__SetMenuItem);
INITIALIZE_EQGAME_OFFSET(CContextMenuManager__AddMenu);
INITIALIZE_EQGAME_OFFSET(CContextMenuManager__CreateDefaultMenu);
INITIALIZE_EQGAME_OFFSET(CContextMenuManager__Flush);
INITIALIZE_EQGAME_OFFSET(CContextMenuManager__GetMenu);
INITIALIZE_EQGAME_OFFSET(CContextMenuManager__PopupMenu);
INITIALIZE_EQGAME_OFFSET(CContextMenuManager__RemoveMenu);
INITIALIZE_EQGAME_OFFSET(CCursorAttachment__AttachToCursor);
INITIALIZE_EQGAME_OFFSET(CCursorAttachment__AttachToCursor1);
INITIALIZE_EQGAME_OFFSET(CCursorAttachment__Deactivate);
INITIALIZE_EQGAME_OFFSET(CDBStr__GetString);
INITIALIZE_EQGAME_OFFSET(CDisplay__cameraType);
INITIALIZE_EQGAME_OFFSET(CDisplay__CleanGameUI);
INITIALIZE_EQGAME_OFFSET(CDisplay__GetClickedActor);
INITIALIZE_EQGAME_OFFSET(CDisplay__GetFloorHeight);
INITIALIZE_EQGAME_OFFSET(CDisplay__GetUserDefinedColor);
INITIALIZE_EQGAME_OFFSET(CDisplay__GetWorldFilePath);
INITIALIZE_EQGAME_OFFSET(CDisplay__is3dON);
INITIALIZE_EQGAME_OFFSET(CDisplay__PreZoneMainUI);
INITIALIZE_EQGAME_OFFSET(CDisplay__RealRender_World);
INITIALIZE_EQGAME_OFFSET(CDisplay__ReloadUI);
INITIALIZE_EQGAME_OFFSET(CDisplay__SetRenderWindow);
INITIALIZE_EQGAME_OFFSET(CDisplay__SetViewActor);
INITIALIZE_EQGAME_OFFSET(CDisplay__ToggleScreenshotMode);
INITIALIZE_EQGAME_OFFSET(CDisplay__TrueDistance);
INITIALIZE_EQGAME_OFFSET(CDisplay__WriteTextHD2);
INITIALIZE_EQGAME_OFFSET(CDisplay__ZoneMainUI);
INITIALIZE_EQGAME_OFFSET(CDistillerInfo__GetIDFromRecordNum);
INITIALIZE_EQGAME_OFFSET(CDistillerInfo__Instance);
INITIALIZE_EQGAME_OFFSET(CEditBaseWnd__SetSel);
INITIALIZE_EQGAME_OFFSET(CEditWnd__DrawCaret);
INITIALIZE_EQGAME_OFFSET(CEditWnd__EnsureCaretVisible);
INITIALIZE_EQGAME_OFFSET(CEditWnd__GetCaretPt);
INITIALIZE_EQGAME_OFFSET(CEditWnd__GetCharIndexPt);
INITIALIZE_EQGAME_OFFSET(CEditWnd__GetDisplayString);
INITIALIZE_EQGAME_OFFSET(CEditWnd__GetHorzOffset);
INITIALIZE_EQGAME_OFFSET(CEditWnd__GetLineForPrintableChar);
INITIALIZE_EQGAME_OFFSET(CEditWnd__GetSelStartPt);
INITIALIZE_EQGAME_OFFSET(CEditWnd__GetSTMLSafeText);
INITIALIZE_EQGAME_OFFSET(CEditWnd__PointFromPrintableChar);
INITIALIZE_EQGAME_OFFSET(CEditWnd__ReplaceSelection);
INITIALIZE_EQGAME_OFFSET(CEditWnd__SelectableCharFromPoint);
INITIALIZE_EQGAME_OFFSET(CEditWnd__SetEditable);
INITIALIZE_EQGAME_OFFSET(CEditWnd__SetWindowText);
INITIALIZE_EQGAME_OFFSET(CEQSuiteTextureLoader__CreateTexture);
INITIALIZE_EQGAME_OFFSET(CEQSuiteTextureLoader__GetDefaultUIPath);
INITIALIZE_EQGAME_OFFSET(CEQSuiteTextureLoader__GetTexture);
INITIALIZE_EQGAME_OFFSET(CEverQuest__ClickedPlayer);
INITIALIZE_EQGAME_OFFSET(CEverQuest__CreateTargetIndicator);
INITIALIZE_EQGAME_OFFSET(CEverQuest__DeleteTargetIndicator);
INITIALIZE_EQGAME_OFFSET(CEverQuest__DoPercentConvert);
INITIALIZE_EQGAME_OFFSET(CEverQuest__DoTellWindow);
INITIALIZE_EQGAME_OFFSET(CEverQuest__DropHeldItemOnGround);
INITIALIZE_EQGAME_OFFSET(CEverQuest__dsp_chat);
INITIALIZE_EQGAME_OFFSET(CEverQuest__Emote);
INITIALIZE_EQGAME_OFFSET(CEverQuest__EnterZone);
INITIALIZE_EQGAME_OFFSET(CEverQuest__GetBodyTypeDesc);
INITIALIZE_EQGAME_OFFSET(CEverQuest__GetClassDesc);
INITIALIZE_EQGAME_OFFSET(CEverQuest__GetClassThreeLetterCode);
INITIALIZE_EQGAME_OFFSET(CEverQuest__GetDeityDesc);
INITIALIZE_EQGAME_OFFSET(CEverQuest__GetLangDesc);
INITIALIZE_EQGAME_OFFSET(CEverQuest__GetRaceDesc);
INITIALIZE_EQGAME_OFFSET(CEverQuest__InterpretCmd);
INITIALIZE_EQGAME_OFFSET(CEverQuest__IssuePetCommand);
INITIALIZE_EQGAME_OFFSET(CEverQuest__LeftClickedOnPlayer);
INITIALIZE_EQGAME_OFFSET(CEverQuest__LMouseUp);
INITIALIZE_EQGAME_OFFSET(CEverQuest__OutputTextToLog);
INITIALIZE_EQGAME_OFFSET(CEverQuest__ReportSuccessfulHeal);
INITIALIZE_EQGAME_OFFSET(CEverQuest__ReportSuccessfulHit);
INITIALIZE_EQGAME_OFFSET(CEverQuest__RightClickedOnPlayer);
INITIALIZE_EQGAME_OFFSET(CEverQuest__RMouseUp);
INITIALIZE_EQGAME_OFFSET(CEverQuest__SetGameState);
INITIALIZE_EQGAME_OFFSET(CEverQuest__trimName);
INITIALIZE_EQGAME_OFFSET(CEverQuest__UPCNotificationFlush);
INITIALIZE_EQGAME_OFFSET(CFindItemWnd__CFindItemWnd);
INITIALIZE_EQGAME_OFFSET(CFindItemWnd__PickupSelectedItem);
INITIALIZE_EQGAME_OFFSET(CFindItemWnd__Update);
INITIALIZE_EQGAME_OFFSET(CFindItemWnd__WndNotification);
INITIALIZE_EQGAME_OFFSET(CGaugeWnd__CalcFillRect);
INITIALIZE_EQGAME_OFFSET(CGaugeWnd__CalcLinesFillRect);
INITIALIZE_EQGAME_OFFSET(CGaugeWnd__Draw);
INITIALIZE_EQGAME_OFFSET(CGroupWnd__UpdateDisplay);
INITIALIZE_EQGAME_OFFSET(CGroupWnd__WndNotification);
INITIALIZE_EQGAME_OFFSET(CGuild__FindMemberByName);
INITIALIZE_EQGAME_OFFSET(CGuild__GetGuildIndex);
INITIALIZE_EQGAME_OFFSET(CGuild__GetGuildName);
INITIALIZE_EQGAME_OFFSET(CharacterBase__GetItemByGlobalIndex);
INITIALIZE_EQGAME_OFFSET(CharacterBase__GetItemByGlobalIndex1);
INITIALIZE_EQGAME_OFFSET(CharacterBase__GetMemorizedSpell);
INITIALIZE_EQGAME_OFFSET(CharacterBase__IsExpansionFlag);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__BardCastBard);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__CalcAffectChange);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__CalcAffectChangeGeneric);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__CanUseItem);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__CanUseMemorizedSpellSlot);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__CastSpell);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__CharacterZoneClient);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__Cur_HP);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__Cur_Mana);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__FindAffectSlot);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__FindItemByGuid);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__FindItemByRecord);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetAdjustedSkill);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetBaseSkill);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetCastingTimeModifier);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetCurrentMod);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetCursorItemCount);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetEffect);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetEnduranceRegen);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetFirstEffectSlot);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetFocusCastingTimeModifier);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetFocusDurationMod);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetFocusRangeModifier);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetFocusReuseMod);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetHPRegen);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetItemCountInInventory);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetItemCountWorn);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetLastEffectSlot);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetManaRegen);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetMaxEffects);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetModCap);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetOpenEffectSlot);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__GetPCSpellAffect);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__HasSkill);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__IsStackBlocked);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__MakeMeVisible);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__Max_Endurance);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__Max_HP);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__Max_Mana);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__NotifyPCAffectChange);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__RemovePCAffectex);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__SpellDuration);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__TotalEffect);
INITIALIZE_EQGAME_OFFSET(CharacterZoneClient__UseSkill);
INITIALIZE_EQGAME_OFFSET(ChatManagerClient__Instance);
INITIALIZE_EQGAME_OFFSET(CHelpWnd__SetFile);
INITIALIZE_EQGAME_OFFSET(CHotButton__SetButtonSize);
INITIALIZE_EQGAME_OFFSET(CHotButton__SetCheck);
INITIALIZE_EQGAME_OFFSET(CHotButtonWnd__DoHotButton);
INITIALIZE_EQGAME_OFFSET(CInvSlot__GetItemBase);
INITIALIZE_EQGAME_OFFSET(CInvSlot__HandleRButtonUp);
INITIALIZE_EQGAME_OFFSET(CInvSlot__SliderComplete);
INITIALIZE_EQGAME_OFFSET(CInvSlot__UpdateItem);
INITIALIZE_EQGAME_OFFSET(CInvSlotMgr__FindInvSlot);
INITIALIZE_EQGAME_OFFSET(CInvSlotMgr__MoveItem);
INITIALIZE_EQGAME_OFFSET(CInvSlotMgr__SelectSlot);
INITIALIZE_EQGAME_OFFSET(CInvSlotWnd__CInvSlotWnd);
INITIALIZE_EQGAME_OFFSET(CInvSlotWnd__DrawTooltip);
INITIALIZE_EQGAME_OFFSET(CInvSlotWnd__HandleLButtonUp);
INITIALIZE_EQGAME_OFFSET(CItemDisplayManager__CreateWindowInstance);
INITIALIZE_EQGAME_OFFSET(CItemDisplayWnd__AboutToShow);
INITIALIZE_EQGAME_OFFSET(CItemDisplayWnd__CItemDisplayWnd);
INITIALIZE_EQGAME_OFFSET(CItemDisplayWnd__dCItemDisplayWnd);
INITIALIZE_EQGAME_OFFSET(CItemDisplayWnd__InsertAugmentRequest);
INITIALIZE_EQGAME_OFFSET(CItemDisplayWnd__RemoveAugmentRequest);
INITIALIZE_EQGAME_OFFSET(CItemDisplayWnd__RequestConvertItem);
INITIALIZE_EQGAME_OFFSET(CItemDisplayWnd__SetItem);
INITIALIZE_EQGAME_OFFSET(CItemDisplayWnd__SetSpell);
INITIALIZE_EQGAME_OFFSET(CItemDisplayWnd__UpdateStrings);
INITIALIZE_EQGAME_OFFSET(CItemDisplayWnd__WndNotification);
INITIALIZE_EQGAME_OFFSET(CKeyRingWnd__ExecuteRightClick);
INITIALIZE_EQGAME_OFFSET(CLabel__UpdateText);
INITIALIZE_EQGAME_OFFSET(CLargeDialogWnd__Open);
INITIALIZE_EQGAME_OFFSET(ClientSOIManager__GetSingleton);
INITIALIZE_EQGAME_OFFSET(CListWnd__AddColumn);
INITIALIZE_EQGAME_OFFSET(CListWnd__AddColumn1);
INITIALIZE_EQGAME_OFFSET(CListWnd__AddLine);
INITIALIZE_EQGAME_OFFSET(CListWnd__AddString);
INITIALIZE_EQGAME_OFFSET(CListWnd__CalculateCustomWindowPositions);
INITIALIZE_EQGAME_OFFSET(CListWnd__CalculateFirstVisibleLine);
INITIALIZE_EQGAME_OFFSET(CListWnd__CalculateVSBRange);
INITIALIZE_EQGAME_OFFSET(CListWnd__ClearAllSel);
INITIALIZE_EQGAME_OFFSET(CListWnd__ClearSel);
INITIALIZE_EQGAME_OFFSET(CListWnd__CListWnd);
INITIALIZE_EQGAME_OFFSET(CListWnd__CloseAndUpdateEditWindow);
INITIALIZE_EQGAME_OFFSET(CListWnd__Compare);
INITIALIZE_EQGAME_OFFSET(CListWnd__dCListWnd);
INITIALIZE_EQGAME_OFFSET(CListWnd__Draw);
INITIALIZE_EQGAME_OFFSET(CListWnd__DrawColumnSeparators);
INITIALIZE_EQGAME_OFFSET(CListWnd__DrawHeader);
INITIALIZE_EQGAME_OFFSET(CListWnd__DrawItem);
INITIALIZE_EQGAME_OFFSET(CListWnd__DrawLine);
INITIALIZE_EQGAME_OFFSET(CListWnd__DrawSeparator);
INITIALIZE_EQGAME_OFFSET(CListWnd__EnableLine);
INITIALIZE_EQGAME_OFFSET(CListWnd__EnsureVisible);
INITIALIZE_EQGAME_OFFSET(CListWnd__ExtendSel);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetColumnMinWidth);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetColumnTooltip);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetColumnWidth);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetCurSel);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetItemAtPoint);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetItemAtPoint1);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetItemData);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetItemHeight);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetItemIcon);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetItemRect);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetItemText);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetItemWnd);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetSelList);
INITIALIZE_EQGAME_OFFSET(CListWnd__GetSeparatorRect);
INITIALIZE_EQGAME_OFFSET(CListWnd__InsertLine);
INITIALIZE_EQGAME_OFFSET(CListWnd__RemoveLine);
INITIALIZE_EQGAME_OFFSET(CListWnd__SetColors);
INITIALIZE_EQGAME_OFFSET(CListWnd__SetColumnJustification);
INITIALIZE_EQGAME_OFFSET(CListWnd__SetColumnLabel);
INITIALIZE_EQGAME_OFFSET(CListWnd__SetColumnsSizable);
INITIALIZE_EQGAME_OFFSET(CListWnd__SetColumnWidth);
INITIALIZE_EQGAME_OFFSET(CListWnd__SetCurSel);
INITIALIZE_EQGAME_OFFSET(CListWnd__SetItemColor);
INITIALIZE_EQGAME_OFFSET(CListWnd__SetItemData);
INITIALIZE_EQGAME_OFFSET(CListWnd__SetItemIcon);
INITIALIZE_EQGAME_OFFSET(CListWnd__SetItemText);
INITIALIZE_EQGAME_OFFSET(CListWnd__SetItemWnd);
INITIALIZE_EQGAME_OFFSET(CListWnd__SetVScrollPos);
INITIALIZE_EQGAME_OFFSET(CListWnd__ShiftColumnSeparator);
INITIALIZE_EQGAME_OFFSET(CListWnd__Sort);
INITIALIZE_EQGAME_OFFSET(CListWnd__ToggleSel);
INITIALIZE_EQGAME_OFFSET(CListWnd__vftable);
INITIALIZE_EQGAME_OFFSET(CLootWnd__LootAll);
INITIALIZE_EQGAME_OFFSET(CLootWnd__RequestLootSlot);
INITIALIZE_EQGAME_OFFSET(CMapViewWnd__CMapViewWnd);
INITIALIZE_EQGAME_OFFSET(CMemoryMappedFile__SetFile);
INITIALIZE_EQGAME_OFFSET(CMerchantWnd__DisplayBuyOrSellPrice);
INITIALIZE_EQGAME_OFFSET(CMerchantWnd__PurchasePageHandler__RequestGetItem);
INITIALIZE_EQGAME_OFFSET(CMerchantWnd__PurchasePageHandler__RequestPutItem);
INITIALIZE_EQGAME_OFFSET(CMerchantWnd__PurchasePageHandler__UpdateList);
INITIALIZE_EQGAME_OFFSET(CMerchantWnd__SelectBuySellSlot);
INITIALIZE_EQGAME_OFFSET(COptionsWnd__FillChatFilterList);
INITIALIZE_EQGAME_OFFSET(CPacketScrambler__hton);
INITIALIZE_EQGAME_OFFSET(CPacketScrambler__ntoh);
INITIALIZE_EQGAME_OFFSET(CPageWnd__FlashTab);
INITIALIZE_EQGAME_OFFSET(CPageWnd__GetTabText);
INITIALIZE_EQGAME_OFFSET(CPageWnd__SetTabText);
INITIALIZE_EQGAME_OFFSET(CQuantityWnd__Open);
INITIALIZE_EQGAME_OFFSET(CResolutionHandler__GetWindowedStyle);
INITIALIZE_EQGAME_OFFSET(CResolutionHandler__UpdateResolution);
INITIALIZE_EQGAME_OFFSET(CScreenPieceTemplate__IsType);
INITIALIZE_EQGAME_OFFSET(CSidlManager__CreateHotButtonWnd);
INITIALIZE_EQGAME_OFFSET(CSidlManager__CreateXWnd);
INITIALIZE_EQGAME_OFFSET(CSidlManagerBase__CreateXWnd);
INITIALIZE_EQGAME_OFFSET(CSidlManagerBase__CreateXWndFromTemplate);
INITIALIZE_EQGAME_OFFSET(CSidlManagerBase__CreateXWndFromTemplate1);
INITIALIZE_EQGAME_OFFSET(CSidlManagerBase__FindAnimation1);
INITIALIZE_EQGAME_OFFSET(CSidlManagerBase__FindButtonDrawTemplate);
INITIALIZE_EQGAME_OFFSET(CSidlManagerBase__FindButtonDrawTemplate1);
INITIALIZE_EQGAME_OFFSET(CSidlManagerBase__FindScreenPieceTemplate);
INITIALIZE_EQGAME_OFFSET(CSidlManagerBase__FindScreenPieceTemplate1);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__CalculateHSBRange);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__CalculateVSBRange);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__ConvertToRes);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__CreateChildrenFromSidl);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__CSidlScreenWnd1);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__CSidlScreenWnd2);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__dCSidlScreenWnd);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__DrawSidlPiece);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__EnableIniStorage);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__GetChildItem);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__GetSidlPiece);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__Init1);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__LoadIniListWnd);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__LoadSidlScreen);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__m_layoutCopy);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__StoreIniVis);
INITIALIZE_EQGAME_OFFSET(CSidlScreenWnd__vftable);
INITIALIZE_EQGAME_OFFSET(CSkillMgr__GetNameToken);
INITIALIZE_EQGAME_OFFSET(CSkillMgr__GetSkillCap);
INITIALIZE_EQGAME_OFFSET(CSkillMgr__IsActivatedSkill);
INITIALIZE_EQGAME_OFFSET(CSkillMgr__IsAvailable);
INITIALIZE_EQGAME_OFFSET(CSkillMgr__IsCombatSkill);
INITIALIZE_EQGAME_OFFSET(CSliderWnd__GetValue);
INITIALIZE_EQGAME_OFFSET(CSliderWnd__SetNumTicks);
INITIALIZE_EQGAME_OFFSET(CSliderWnd__SetValue);
INITIALIZE_EQGAME_OFFSET(CSpellBookWnd__MemorizeSet);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__AppendSTML);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__CalculateHSBRange);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__CalculateVSBRange);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__CanBreakAtCharacter);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__FastForwardToEndOfTag);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__ForceParseNow);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__GetNextTagPiece);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__GetVisibleText);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__InitializeWindowVariables);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__MakeStmlColorTag);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__MakeWndNotificationTag);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__SetSTMLText);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__StripFirstSTMLLines);
INITIALIZE_EQGAME_OFFSET(CStmlWnd__UpdateHistoryString);
INITIALIZE_EQGAME_OFFSET(CTabWnd__Draw);
INITIALIZE_EQGAME_OFFSET(CTabWnd__DrawCurrentPage);
INITIALIZE_EQGAME_OFFSET(CTabWnd__DrawTab);
INITIALIZE_EQGAME_OFFSET(CTabWnd__GetCurrentPage);
INITIALIZE_EQGAME_OFFSET(CTabWnd__GetCurrentTabIndex);
INITIALIZE_EQGAME_OFFSET(CTabWnd__GetPageFromTabIndex);
INITIALIZE_EQGAME_OFFSET(CTabWnd__GetPageInnerRect);
INITIALIZE_EQGAME_OFFSET(CTabWnd__GetTabInnerRect);
INITIALIZE_EQGAME_OFFSET(CTabWnd__GetTabRect);
INITIALIZE_EQGAME_OFFSET(CTabWnd__InsertPage);
INITIALIZE_EQGAME_OFFSET(CTabWnd__RemovePage);
INITIALIZE_EQGAME_OFFSET(CTabWnd__SetPage);
INITIALIZE_EQGAME_OFFSET(CTabWnd__SetPageRect);
INITIALIZE_EQGAME_OFFSET(CTabWnd__UpdatePage);
INITIALIZE_EQGAME_OFFSET(CTargetManager__Get);
INITIALIZE_EQGAME_OFFSET(CTargetRing__Cast);
INITIALIZE_EQGAME_OFFSET(CTargetWnd__GetBuffCaster);
INITIALIZE_EQGAME_OFFSET(CTargetWnd__HandleBuffRemoveRequest);
INITIALIZE_EQGAME_OFFSET(CTargetWnd__RefreshTargetBuffs);
INITIALIZE_EQGAME_OFFSET(CTargetWnd__WndNotification);
INITIALIZE_EQGAME_OFFSET(CTaskManager__GetElementDescription);
INITIALIZE_EQGAME_OFFSET(CTaskManager__GetEntry);
INITIALIZE_EQGAME_OFFSET(CTaskManager__GetTaskStatus);
INITIALIZE_EQGAME_OFFSET(CTaskWnd__ConfirmAbandonTask);
INITIALIZE_EQGAME_OFFSET(CTaskWnd__UpdateTaskTimers);
INITIALIZE_EQGAME_OFFSET(CTextOverlay__DisplayText);
INITIALIZE_EQGAME_OFFSET(CTextureAnimation__Draw);
INITIALIZE_EQGAME_OFFSET(CTextureAnimation__SetCurCell);
INITIALIZE_EQGAME_OFFSET(CTextureFont__DrawWrappedText);
INITIALIZE_EQGAME_OFFSET(CTextureFont__DrawWrappedText1);
INITIALIZE_EQGAME_OFFSET(CTextureFont__DrawWrappedText2);
INITIALIZE_EQGAME_OFFSET(CTextureFont__GetTextExtent);
INITIALIZE_EQGAME_OFFSET(CUnSerializeBuffer__GetString);
INITIALIZE_EQGAME_OFFSET(CWndDisplayManager__FindWindowA);
INITIALIZE_EQGAME_OFFSET(CXMLDataManager__GetXMLData);
INITIALIZE_EQGAME_OFFSET(CXMLSOMDocumentBase__XMLRead);
INITIALIZE_EQGAME_OFFSET(CXStr__CXStr);
INITIALIZE_EQGAME_OFFSET(CXStr__CXStr1);
INITIALIZE_EQGAME_OFFSET(CXStr__CXStr3);
INITIALIZE_EQGAME_OFFSET(CXStr__dCXStr);
INITIALIZE_EQGAME_OFFSET(CXStr__Delete);
INITIALIZE_EQGAME_OFFSET(CXStr__FindNext);
INITIALIZE_EQGAME_OFFSET(CXStr__gCXStrAccess);
INITIALIZE_EQGAME_OFFSET(CXStr__GetChar);
INITIALIZE_EQGAME_OFFSET(CXStr__GetUnicode);
INITIALIZE_EQGAME_OFFSET(CXStr__gFreeLists);
INITIALIZE_EQGAME_OFFSET(CXStr__Insert);
INITIALIZE_EQGAME_OFFSET(CXStr__operator_char_p);
INITIALIZE_EQGAME_OFFSET(CXStr__operator_equal);
INITIALIZE_EQGAME_OFFSET(CXStr__operator_equal1);
INITIALIZE_EQGAME_OFFSET(CXStr__operator_plus_equal1);
INITIALIZE_EQGAME_OFFSET(CXStr__SetString);
INITIALIZE_EQGAME_OFFSET(CXWnd__BringToTop);
INITIALIZE_EQGAME_OFFSET(CXWnd__ClrFocus);
INITIALIZE_EQGAME_OFFSET(CXWnd__CXWnd);
INITIALIZE_EQGAME_OFFSET(CXWnd__dCXWnd);
INITIALIZE_EQGAME_OFFSET(CXWnd__Destroy);
INITIALIZE_EQGAME_OFFSET(CXWnd__DoAllDrawing);
INITIALIZE_EQGAME_OFFSET(CXWnd__DrawChildren);
INITIALIZE_EQGAME_OFFSET(CXWnd__DrawColoredRect);
INITIALIZE_EQGAME_OFFSET(CXWnd__DrawTooltip);
INITIALIZE_EQGAME_OFFSET(CXWnd__DrawTooltipAtPoint);
INITIALIZE_EQGAME_OFFSET(CXWnd__GetBorderFrame);
INITIALIZE_EQGAME_OFFSET(CXWnd__GetChildItem);
INITIALIZE_EQGAME_OFFSET(CXWnd__GetChildWndAt);
INITIALIZE_EQGAME_OFFSET(CXWnd__GetClientClipRect);
INITIALIZE_EQGAME_OFFSET(CXWnd__GetRelativeRect);
INITIALIZE_EQGAME_OFFSET(CXWnd__GetScreenClipRect);
INITIALIZE_EQGAME_OFFSET(CXWnd__GetScreenRect);
INITIALIZE_EQGAME_OFFSET(CXWnd__GetTooltipRect);
INITIALIZE_EQGAME_OFFSET(CXWnd__GetWindowTextA);
INITIALIZE_EQGAME_OFFSET(CXWnd__IsActive);
INITIALIZE_EQGAME_OFFSET(CXWnd__IsDescendantOf);
INITIALIZE_EQGAME_OFFSET(CXWnd__IsReallyVisible);
INITIALIZE_EQGAME_OFFSET(CXWnd__IsType);
INITIALIZE_EQGAME_OFFSET(CXWnd__Minimize);
INITIALIZE_EQGAME_OFFSET(CXWnd__Move);
INITIALIZE_EQGAME_OFFSET(CXWnd__Move1);
INITIALIZE_EQGAME_OFFSET(CXWnd__ProcessTransition);
INITIALIZE_EQGAME_OFFSET(CXWnd__Refade);
INITIALIZE_EQGAME_OFFSET(CXWnd__Resize);
INITIALIZE_EQGAME_OFFSET(CXWnd__Right);
INITIALIZE_EQGAME_OFFSET(CXWnd__SetFocus);
INITIALIZE_EQGAME_OFFSET(CXWnd__SetFont);
INITIALIZE_EQGAME_OFFSET(CXWnd__SetKeyTooltip);
INITIALIZE_EQGAME_OFFSET(CXWnd__SetMouseOver);
INITIALIZE_EQGAME_OFFSET(CXWnd__SetParent);
INITIALIZE_EQGAME_OFFSET(CXWnd__StartFade);
INITIALIZE_EQGAME_OFFSET(CXWnd__vftable);
INITIALIZE_EQGAME_OFFSET(CXWndManager__DrawCursor);
INITIALIZE_EQGAME_OFFSET(CXWndManager__DrawWindows);
INITIALIZE_EQGAME_OFFSET(CXWndManager__GetKeyboardFlags);
INITIALIZE_EQGAME_OFFSET(CXWndManager__HandleKeyboardMsg);
INITIALIZE_EQGAME_OFFSET(CXWndManager__RemoveWnd);
INITIALIZE_EQGAME_OFFSET(DrawNetStatus);
INITIALIZE_EQGAME_OFFSET(EQ_Item__CanDrop);
INITIALIZE_EQGAME_OFFSET(EQ_Item__CanGemFitInSlot);
INITIALIZE_EQGAME_OFFSET(EQ_Item__CanGoInBag);
INITIALIZE_EQGAME_OFFSET(EQ_Item__CreateItemClient);
INITIALIZE_EQGAME_OFFSET(EQ_Item__CreateItemTagString);
INITIALIZE_EQGAME_OFFSET(EQ_Item__GetImageNum);
INITIALIZE_EQGAME_OFFSET(EQ_Item__GetItemValue);
INITIALIZE_EQGAME_OFFSET(EQ_Item__IsEmpty);
INITIALIZE_EQGAME_OFFSET(EQ_Item__IsKeyRingItem);
INITIALIZE_EQGAME_OFFSET(EQ_Item__IsStackable);
INITIALIZE_EQGAME_OFFSET(EQ_Item__ValueSellMerchant);
INITIALIZE_EQGAME_OFFSET(EQ_LoadingS__Array);
INITIALIZE_EQGAME_OFFSET(EQ_LoadingS__SetProgressBar);
INITIALIZE_EQGAME_OFFSET(EQ_PC__AlertInventoryChanged);
INITIALIZE_EQGAME_OFFSET(EQ_PC__GetAlternateAbilityId);
INITIALIZE_EQGAME_OFFSET(EQ_PC__GetCombatAbility);
INITIALIZE_EQGAME_OFFSET(EQ_PC__GetCombatAbilityTimer);
INITIALIZE_EQGAME_OFFSET(EQ_PC__GetItemContainedRealEstateIds);
INITIALIZE_EQGAME_OFFSET(EQ_PC__GetItemRecastTimer);
INITIALIZE_EQGAME_OFFSET(EQ_PC__GetKeyRingItems);
INITIALIZE_EQGAME_OFFSET(EQ_PC__GetNonArchivedOwnedRealEstates);
INITIALIZE_EQGAME_OFFSET(EQ_PC__GetPcZoneClient);
INITIALIZE_EQGAME_OFFSET(EQ_PC__HasLoreItem);
INITIALIZE_EQGAME_OFFSET(EQ_PC__RemoveMyAffect);
INITIALIZE_EQGAME_OFFSET(EQ_Spell__GetSpellAffectByIndex);
INITIALIZE_EQGAME_OFFSET(EQ_Spell__GetSpellAffectBySlot);
INITIALIZE_EQGAME_OFFSET(EQ_Spell__GetSpellLevelNeeded);
INITIALIZE_EQGAME_OFFSET(EQ_Spell__IsDegeneratingLevelMod);
INITIALIZE_EQGAME_OFFSET(EQ_Spell__IsSPAIgnoredByStacking);
INITIALIZE_EQGAME_OFFSET(EQ_Spell__IsSPAStacking);
INITIALIZE_EQGAME_OFFSET(EQ_Spell__SpellAffectBase);
INITIALIZE_EQGAME_OFFSET(EQ_Spell__SpellAffects);
INITIALIZE_EQGAME_OFFSET(EQGroundItemListManager__Instance);
INITIALIZE_EQGAME_OFFSET(EQItemList__add_item);
INITIALIZE_EQGAME_OFFSET(EQItemList__delete_item);
INITIALIZE_EQGAME_OFFSET(EQItemList__EQItemList);
INITIALIZE_EQGAME_OFFSET(EQItemList__FreeItemList);
INITIALIZE_EQGAME_OFFSET(EQMisc__GetActiveFavorCost);
INITIALIZE_EQGAME_OFFSET(EQPlacedItemManager__GetItemByGuid);
INITIALIZE_EQGAME_OFFSET(EQPlacedItemManager__GetItemByRealEstateAndRealEstateItemIds);
INITIALIZE_EQGAME_OFFSET(EQPlacedItemManager__Instance);
INITIALIZE_EQGAME_OFFSET(EQPlayer__CanSee);
INITIALIZE_EQGAME_OFFSET(EQPlayer__CanSee1);
INITIALIZE_EQGAME_OFFSET(EQPlayer__ChangeBoneStringSprite);
INITIALIZE_EQGAME_OFFSET(EQPlayer__dEQPlayer);
INITIALIZE_EQGAME_OFFSET(EQPlayer__DoAttack);
INITIALIZE_EQGAME_OFFSET(EQPlayer__EQPlayer);
INITIALIZE_EQGAME_OFFSET(EQPlayer__IsTargetable);
INITIALIZE_EQGAME_OFFSET(EQPlayer__SetNameSpriteState);
INITIALIZE_EQGAME_OFFSET(EQPlayer__SetNameSpriteTint);
INITIALIZE_EQGAME_OFFSET(EQPlayerManager__GetPlayerFromPartialName);
INITIALIZE_EQGAME_OFFSET(EQPlayerManager__GetSpawnByID);
INITIALIZE_EQGAME_OFFSET(EQPlayerManager__GetSpawnByName);
INITIALIZE_EQGAME_OFFSET(EqSoundManager__PlayScriptMp3);
INITIALIZE_EQGAME_OFFSET(EqSoundManager__SoundAssistPlay);
INITIALIZE_EQGAME_OFFSET(EqSoundManager__WaveInstancePlay);
INITIALIZE_EQGAME_OFFSET(EqSoundManager__WavePlay);
INITIALIZE_EQGAME_OFFSET(EQSpellStrings__GetString);
INITIALIZE_EQGAME_OFFSET(EQSwitch__UseSwitch);
INITIALIZE_EQGAME_OFFSET(EverQuest__Cameras);
INITIALIZE_EQGAME_OFFSET(EverQuestinfo__IsItemPending);
INITIALIZE_EQGAME_OFFSET(Expansion_HoT);
INITIALIZE_EQGAME_OFFSET(FactionManagerClient__HandleFactionMessage);
INITIALIZE_EQGAME_OFFSET(FactionManagerClient__Instance);
INITIALIZE_EQGAME_OFFSET(IconCache__GetIcon);
INITIALIZE_EQGAME_OFFSET(IString__Append);
INITIALIZE_EQGAME_OFFSET(ItemBase__IsLore);
INITIALIZE_EQGAME_OFFSET(ItemBase__IsLoreEquipped);
INITIALIZE_EQGAME_OFFSET(ItemBaseContainer__CreateItemGlobalIndex);
INITIALIZE_EQGAME_OFFSET(ItemBaseContainer__ItemBaseContainer);
INITIALIZE_EQGAME_OFFSET(ItemClient__dItemClient);
INITIALIZE_EQGAME_OFFSET(KeyCombo__GetTextDescription);
INITIALIZE_EQGAME_OFFSET(KeypressHandler__AttachAltKeyToEqCommand);
INITIALIZE_EQGAME_OFFSET(KeypressHandler__AttachKeyToEqCommand);
INITIALIZE_EQGAME_OFFSET(KeypressHandler__ClearCommandStateArray);
INITIALIZE_EQGAME_OFFSET(KeypressHandler__Get);
INITIALIZE_EQGAME_OFFSET(KeypressHandler__HandleKeyDown);
INITIALIZE_EQGAME_OFFSET(KeypressHandler__HandleKeyUp);
INITIALIZE_EQGAME_OFFSET(KeypressHandler__SaveKeymapping);
INITIALIZE_EQGAME_OFFSET(LootFiltersManager__AddItemLootFilter);
INITIALIZE_EQGAME_OFFSET(LootFiltersManager__GetItemFilterData);
INITIALIZE_EQGAME_OFFSET(LootFiltersManager__RemoveItemLootFilter);
INITIALIZE_EQGAME_OFFSET(LootFiltersManager__SetItemLootFilter);
INITIALIZE_EQGAME_OFFSET(MapViewMap__Clear);
INITIALIZE_EQGAME_OFFSET(MapViewMap__dMapViewMap);
INITIALIZE_EQGAME_OFFSET(MapViewMap__GetWorldCoordinates);
INITIALIZE_EQGAME_OFFSET(MapViewMap__MapViewMap);
INITIALIZE_EQGAME_OFFSET(MapViewMap__SaveEx);
INITIALIZE_EQGAME_OFFSET(MapViewMap__SetZoom);
INITIALIZE_EQGAME_OFFSET(MapViewMap__vftable);
INITIALIZE_EQGAME_OFFSET(MercenaryAlternateAdvancementManagerClient__Instance);
INITIALIZE_EQGAME_OFFSET(msg_new_text);
INITIALIZE_EQGAME_OFFSET(msg_spell_worn_off);
INITIALIZE_EQGAME_OFFSET(msgTokenText);
INITIALIZE_EQGAME_OFFSET(MultipleItemMoveManager__ProcessMove);
INITIALIZE_EQGAME_OFFSET(PcClient__GetConLevel);
INITIALIZE_EQGAME_OFFSET(PcClient__PcClient);
INITIALIZE_EQGAME_OFFSET(PcZoneClient__doCombatAbility);
INITIALIZE_EQGAME_OFFSET(PcZoneClient__BandolierSwap);
INITIALIZE_EQGAME_OFFSET(PcZoneClient__CanEquipItem);
INITIALIZE_EQGAME_OFFSET(PcZoneClient__DestroyHeldItemOrMoney);
INITIALIZE_EQGAME_OFFSET(PcZoneClient__GetItemByID);
INITIALIZE_EQGAME_OFFSET(PcZoneClient__GetItemByItemClass);
INITIALIZE_EQGAME_OFFSET(PcZoneClient__GetLinkedSpellReuseTimer);
INITIALIZE_EQGAME_OFFSET(PcZoneClient__GetPcSkillLimit);
INITIALIZE_EQGAME_OFFSET(PcZoneClient__HasAlternateAbility);
INITIALIZE_EQGAME_OFFSET(PcZoneClient__RemoveBuffEffect);
INITIALIZE_EQGAME_OFFSET(PcZoneClient__RemovePetEffect);
INITIALIZE_EQGAME_OFFSET(pinstLootFiltersManager);
INITIALIZE_EQGAME_OFFSET(PlayerBase__GetVisibilityLineSegment);
INITIALIZE_EQGAME_OFFSET(PlayerBase__HasProperty_j);
INITIALIZE_EQGAME_OFFSET(PlayerClient__GetPcClient);
INITIALIZE_EQGAME_OFFSET(PlayerPointManager__GetAltCurrency);
INITIALIZE_EQGAME_OFFSET(PlayerZoneClient__ChangeHeight);
INITIALIZE_EQGAME_OFFSET(PlayerZoneClient__GetLevel);
INITIALIZE_EQGAME_OFFSET(PlayerZoneClient__IsValidTeleport);
INITIALIZE_EQGAME_OFFSET(PlayerZoneClient__LegalPlayerRace);
INITIALIZE_EQGAME_OFFSET(ProfileManager__GetCurrentProfile);
INITIALIZE_EQGAME_OFFSET(RealEstateManagerClient__GetItemByRealEstateAndItemIds);
INITIALIZE_EQGAME_OFFSET(RealEstateManagerClient__Instance);
INITIALIZE_EQGAME_OFFSET(SpellManager__GetSpellByGroupAndRank);
INITIALIZE_EQGAME_OFFSET(Spellmanager__LoadTextSpells);
INITIALIZE_EQGAME_OFFSET(SpellManager__SpellManager);
INITIALIZE_EQGAME_OFFSET(StringTable__getString);
INITIALIZE_EQGAME_OFFSET(Teleport_Table_Size);
INITIALIZE_EQGAME_OFFSET(Teleport_Table);
INITIALIZE_EQGAME_OFFSET(Util__FastTime);

//----------------------------------------------------------------------------
// Instance Pointers
//----------------------------------------------------------------------------

BYTE*                  EQADDR_ATTACK             = nullptr;
CMDLIST*               EQADDR_CMDLIST            = nullptr;
IDirectInputDevice8A** EQADDR_DIKEYBOARD         = nullptr;
DWORD                  EQADDR_DIMAIN             = 0;
IDirectInputDevice8A** EQADDR_DIMOUSE            = nullptr;
POINT*                 EQADDR_DIMOUSECHECK       = nullptr;
POINT*                 EQADDR_DIMOUSECOPY        = nullptr;
int*                   EQADDR_DOABILITYLIST      = nullptr;
DWORD                  EQADDR_GROUPAGGRO         = 0;
void*                  EQADDR_GWORLD             = nullptr;
char*                  EQADDR_LASTTELL           = nullptr;
DWORD                  EQADDR_HWND               = 0;
MQMouseInfo*           EQADDR_MOUSE              = nullptr;
MOUSECLICK*            EQADDR_MOUSECLICK         = nullptr;
BYTE*                  EQADDR_NOTINCHATMODE      = nullptr;
BYTE*                  EQADDR_RUNWALKSTATE       = nullptr;
char*                  EQADDR_SERVERHOST         = nullptr;
char*                  EQADDR_SERVERNAME         = nullptr;
DWORD*                 EQADDR_SUBSCRIPTIONTYPE   = nullptr;
char*                  EQADDR_TARGETAGGROHOLDER  = nullptr;
BYTE*                  EQADDR_ZONETYPE           = nullptr;
char**                 EQMappableCommandList     = nullptr;
BYTE*                  EQbCommandStates          = nullptr;
HMODULE*               ghEQMainInstance          = nullptr;
BYTE*                  gpAutoFire                = nullptr;
AUTOSKILL*             gpAutoSkill               = nullptr;
DWORD*                 gpbCommandEvent           = nullptr;
char*                  gpbRangedAttackReady      = nullptr;
char*                  gpbShowNetStatus          = nullptr;
bool*                  gpbUseTellWindows         = nullptr;
DWORD*                 gpMouseEventTime          = nullptr;
DWORD*                 gpPCNames                 = nullptr;
BYTE*                  gpShiftKeyDown            = nullptr; // addr+1=ctrl, addr+2=alt
DWORD*                 gpShowNames               = nullptr;
CDynamicZone*          pDynamicZone              = nullptr;
EQLogin*               pEQLogin                  = nullptr;
EQMisc*                pEQMisc                   = nullptr;
CEQSuiteTextureLoader* pEQSuiteTextureLoader     = nullptr;
EVERQUESTINFO*         pEverQuestInfo            = nullptr;
INT*                   pgCurrentSocial           = nullptr;
CGuild*                pGuild                    = nullptr;
GUILDS*                pGuildList                = nullptr;
char*                  pMouseLook                = nullptr;
EQRAID*                pRaid                     = nullptr;
DWORD*                 pScreenMode               = nullptr;
DWORD*                 pScreenX                  = nullptr;
DWORD*                 pScreenXMax               = nullptr;
DWORD*                 pScreenY                  = nullptr;
DWORD*                 pScreenYMax               = nullptr;
EQSOCIALCHANGED*       pSocialChangedList        = nullptr;
EQSOCIAL*              pSocialList               = nullptr;
SpellLoadout*          pSpellSets                = nullptr;
CTaskManager*          pTaskManager              = nullptr;
BYTE*                  pTributeActive            = nullptr;
ZONEINFO*              pZoneInfo                 = nullptr;
SoeUtil::String*       pExceptionSubmissionEndpoint = nullptr;

ForeignPointer<PcClient>                         pCharData;          // deprecated
ForeignPointer<PcClient>                         pPCData;            // deprecated
ForeignPointer<PcClient>                         pLocalPC;
ForeignPointer<PlayerClient>                     pActiveBanker;
ForeignPointer<PlayerClient>                     pActiveCorpse;
ForeignPointer<PlayerClient>                     pActiveGMaster;
ForeignPointer<PlayerClient>                     pActiveMerchant;
ForeignPointer<PlayerClient>                     pCharSpawn;         // deprecated
ForeignPointer<PlayerClient>                     pControlledPlayer;
ForeignPointer<PlayerClient>                     pLocalPlayer;
ForeignPointer<PlayerClient>                     pTarget;
ForeignPointer<PlayerClient>                     pTradeTarget;
ComputedPointer<PlayerClient>                    pSpawnList([] { return pSpawnManager ? pSpawnManager->FirstSpawn : nullptr; });

ComputedPointer<AggroMeterManagerClient>         pAggroInfo([]{ return &AggroMeterManagerClient::Instance(); });
ForeignPointer<AltAdvManager>                    pAltAdvManager;
ComputedPointer<ClientAuraManager>               pAuraMgr([]{ return ClientAuraManager::GetSingleton(); });
ForeignPointer<CChatWindowManager>               pChatManager;
ComputedPointer<CChatService>                    pChatService([]{ return (CChatService*)pEverQuest.get_as<EVERQUEST>()->ChatService; });
ForeignPointer<connection_t>                     pConnection;
ForeignPointer<CContainerMgr>                    pContainerMgr;
ForeignPointer<CContextMenuManager>              pContextMenuManager;
ForeignPointer<MAPLABEL>                         pCurrentMapLabel;
ForeignPointer<DatabaseStringTable>              pDBStr;
ForeignPointer<CDisplay>                         pDisplay;
ForeignPointer<DynamicZonePlayerInfo>            pDZMember;
ForeignPointer<DynamicZoneClientTimerData>       pDZTimerInfo;
ForeignPointer<EqSoundManager>                   pEqSoundManager;
ForeignPointer<EQSpellStrings>                   pEQSpellStrings;
ForeignPointer<CEverQuest, EVERQUEST>            pEverQuest;
ForeignPointer<SGraphicsEngine>                  pGraphicsEngine;
ForeignPointer<CItemDisplayManager>              pItemDisplayManager;
ComputedPointer<EQGroundItemListManager>         pItemList([]{ return &EQGroundItemListManager::Instance(); });
ComputedPointer<KeypressHandler>                 pKeypressHandler([]{ return &KeypressHandler::Get(); });
ForeignPointer<LootFiltersManager>               pLootFiltersManager;
ComputedPointer<EQMERCALTABILITIES>              pMercAltAbilities([]{ return (EQMERCALTABILITIES*)&MercenaryAlternateAdvancementManagerClient::Instance(); });
ForeignPointer<CMercenaryManager>                pMercInfo;
ForeignPointer<CMercenaryManager>                pMercManager;
ComputedPointer<CPlayerPointManager>             pPlayerPointManager([]{ return pLocalPC ? (PlayerPointManager*)&pLocalPC->PointManager : nullptr; });
ForeignPointer<CResolutionHandler>               pResolutionHandler;
ForeignPointer<CSidlManager>                     pSidlMgr;
ForeignPointer<SkillManager>                     pSkillMgr;
ForeignPointer<SkillManager>                     pCSkillMgr;
ForeignPointer<PlayerManagerClient>              pSpawnManager;
ForeignPointer<ClientSpellManager, SPELLMGR>     pSpellMgr;
ForeignPointer<StringTable, EQSTRINGTABLE>       pStringTable;
ForeignPointer<EqSwitchManager>                  pSwitchMgr;
ForeignPointer<SharedTaskPlayerInfo>             pTaskMember;
ComputedPointer<CBroadcast>                      pTextOverlay([]{ return CBroadcast::Get(); });
ForeignPointer<CXWndManager>                     pWndMgr;
ForeignPointer<EQWorldData>                      pWorldData;

/* WINDOW INSTANCES */
ForeignPointer<CAAWnd>                           pAAWnd;
ForeignPointer<CAchievementsWnd>                 pAchievementsWnd;
ForeignPointer<CActionsWnd>                      pActionsWnd;
ForeignPointer<CAdvancedLootWnd>                 pAdvancedLootWnd;
ForeignPointer<CAlarmWnd>                        pAlarmWnd;
ForeignPointer<CAuraWnd>                         pAuraWnd;
ForeignPointer<CBandolierWnd>                    pBandolierWnd;
ForeignPointer<CBankWnd>                         pBankWnd;
ForeignPointer<CBarterWnd>                       pBarterWnd;
ForeignPointer<CBarterSearchWnd>                 pBarterSearchWnd;
ForeignPointer<CBazaarSearchWnd>                 pBazaarSearchWnd;
ForeignPointer<CBazaarWnd>                       pBazaarWnd;
ForeignPointer<CBodyTintWnd>                     pBodyTintWnd;
ForeignPointer<CBookWnd>                         pBookWnd;
ForeignPointer<CBreathWnd>                       pBreathWnd;
ForeignPointer<CBuffWindow>                      pBuffWnd;
ForeignPointer<CBuffWindow>                      pSongWnd;
ForeignPointer<CBugReportWnd>                    pBugReportWnd;
ForeignPointer<CCastingWnd>                      pCastingWnd;
ForeignPointer<CCastSpellWnd>                    pCastSpellWnd;
ForeignPointer<CCharacterListWnd>                pCharacterListWnd;
ForeignPointer<CColorPickerWnd>                  pColorPickerWnd;
ForeignPointer<CCombatAbilityWnd>                pCombatAbilityWnd;
ForeignPointer<CCombatSkillsSelectWnd>           pCombatSkillsSelectWnd;
ForeignPointer<CCompassWnd>                      pCompassWnd;
ForeignPointer<CConfirmationDialog>              pConfirmationDialog;
ForeignPointer<CCursorAttachment>                pCursorAttachment;
ForeignPointer<CEditLabelWnd>                    pEditLabelWnd;
ForeignPointer<CEQMainWnd>                       pEQMainWnd;
ForeignPointer<CExtendedTargetWnd>               pExtendedTargetWnd;
ForeignPointer<CFactionWnd>                      pFactionWnd;
ForeignPointer<CFeedbackWnd>                     pFeedbackWnd;
ForeignPointer<CFileSelectionWnd>                pFileSelectionWnd;
ForeignPointer<CFindItemWnd>                     pFindItemWnd;
ForeignPointer<CFindLocationWnd>                 pFindLocationWnd;
ForeignPointer<CFriendsWnd>                      pFriendsWnd;
ForeignPointer<CGemsGameWnd>                     pGemsGameWnd;
ForeignPointer<CGiveWnd>                         pGiveWnd;
ForeignPointer<CGroupSearchFiltersWnd>           pGroupSearchFiltersWnd;
ForeignPointer<CGroupSearchWnd>                  pGroupSearchWnd;
ForeignPointer<CGroupWnd>                        pGroupWnd;
ForeignPointer<CGuildMgmtWnd>                    pGuildMgmtWnd;
ForeignPointer<CHotButtonWnd>                    pHotButtonWnd;
ForeignPointer<CInspectWnd>                      pInspectWnd;
ForeignPointer<CInventoryWnd>                    pInventoryWnd;
ForeignPointer<CInvSlotMgr>                      pInvSlotMgr;
ForeignPointer<CJournalCatWnd>                   pJournalCatWnd;
ForeignPointer<CJournalTextWnd>                  pJournalTextWnd;
ForeignPointer<CKeyRingWnd>                      pKeyRingWnd;
ForeignPointer<CLargeDialogWnd>                  pLargeDialog;
ForeignPointer<CLoadskinWnd>                     pLoadskinWnd;
ForeignPointer<CLootWnd>                         pLootWnd;
ForeignPointer<CMapToolbarWnd>                   pMapToolbarWnd;
ForeignPointer<CMapViewWnd>                      pMapViewWnd;
ForeignPointer<CMarketplaceWnd>                  pMarketplaceWnd;
ForeignPointer<CMerchantWnd>                     pMerchantWnd;
ForeignPointer<CMusicPlayerWnd>                  pMusicPlayerWnd;
ForeignPointer<CNoteWnd>                         pNoteWnd;
ForeignPointer<COptionsWnd>                      pOptionsWnd;
ForeignPointer<COverseerWnd>                     pOverseerWnd;
ForeignPointer<CPetInfoWnd>                      pPetInfoWnd;
ForeignPointer<CPetitionQWnd>                    pPetitionQWnd;
ForeignPointer<CPlayerCustomizationWnd>          pPlayerCustomizationWnd;
ForeignPointer<CPlayerNotesWnd>                  pPlayerNotesWnd;
ForeignPointer<CPlayerWnd>                       pPlayerWnd;
ForeignPointer<CPurchaseGroupWnd>                pPurchaseGroupWnd;
ForeignPointer<CQuantityWnd>                     pQuantityWnd;
ForeignPointer<CRaidOptionsWnd>                  pRaidOptionsWnd;
ForeignPointer<CRaidWnd>                         pRaidWnd;
ForeignPointer<CRealEstateItemsWnd>              pRealEstateItemsWnd;
ForeignPointer<CRespawnWnd>                      pRespawnWnd;
ForeignPointer<CRewardSelectionWnd>              pRewardSelectionWnd;
ForeignPointer<CSelectorWnd>                     pSelectorWnd;
ForeignPointer<CSkillsSelectWnd>                 pSkillsSelectWnd;
ForeignPointer<CSkillsWnd>                       pSkillsWnd;
ForeignPointer<CSocialEditWnd>                   pSocialEditWnd;
ForeignPointer<CSpellBookWnd>                    pSpellBookWnd;
ForeignPointer<CStoryWnd>                        pStoryWnd;
ForeignPointer<CTargetWnd>                       pTargetWnd;
ForeignPointer<CTaskWnd>                         pTaskWnd;
ForeignPointer<CTextEntryWnd>                    pTextEntryWnd;
ForeignPointer<CTimeLeftWnd>                     pTimeLeftWnd;
ForeignPointer<CTipWnd>                          pTipWndCONTEXT;
ForeignPointer<CTipWnd>                          pTipWndOFDAY;
ForeignPointer<CTrackingWnd>                     pTrackingWnd;
ForeignPointer<CTradeWnd>                        pTradeWnd;
ForeignPointer<CTrainWnd>                        pTrainWnd;
ForeignPointer<CVideoModesWnd>                   pVideoModesWnd;
ForeignPointer<CZoneGuideWnd>                    pZoneGuideWnd;

ForeignPointer<DWORD>                            g_pDrawHandler;

fEQNewUIINI            NewUIINI                  = nullptr;
fEQProcGameEvts        ProcessGameEvents         = nullptr;
fGetLabelFromEQ        GetLabelFromEQ            = nullptr;
DWORD                  __ModuleList              = 0;

void InitializeGlobalOffsets()
{
	__ModuleList = (DWORD)GetProcAddress((HMODULE)Kernel32BaseAddress, "K32EnumProcessModules");
}

void InitializeEQGameOffsets()
{
	if (!EQGameBaseAddress)
	{
		return;
	}

	// Raw pointers (value types in eq)
	EQADDR_ATTACK                   = (BYTE*)__Attack;
	EQADDR_CMDLIST                  = (PCMDLIST)__CommandList;
	EQADDR_DIKEYBOARD               = (IDirectInputDevice8A**)DI8__Keyboard;
	EQADDR_DIMAIN                   = DI8__Main;
	EQADDR_DIMOUSE                  = (IDirectInputDevice8A**)DI8__Mouse;
	EQADDR_DIMOUSECHECK             = (PPOINT)DI8__Mouse_Check;
	EQADDR_DIMOUSECOPY              = (PPOINT)DI8__Mouse_Copy;
	EQADDR_DOABILITYLIST            = (int*)__DoAbilityList;
	EQADDR_GROUPAGGRO               = (DWORD)__GroupAggro;
	EQADDR_GWORLD                   = (void*)__gWorld;
	EQADDR_HWND                     = __HWnd;
	EQADDR_LASTTELL                 = (char*)__LastTell;
	EQADDR_MOUSE                    = (MQMouseInfo*)__Mouse;
	EQADDR_MOUSECLICK               = (PMOUSECLICK)__Clicks;
	EQADDR_NOTINCHATMODE            = (BYTE*)__InChatMode;
	EQADDR_RUNWALKSTATE             = (BYTE*)__RunWalkState;
	EQADDR_SERVERHOST               = (char*)__ServerHost;
	EQADDR_SERVERNAME               = (char*)__ServerName;
	EQADDR_SUBSCRIPTIONTYPE         = (PDWORD)__SubscriptionType;
	EQADDR_TARGETAGGROHOLDER        = (char*)__TargetAggroHolder;
	EQADDR_ZONETYPE                 = (BYTE*)__ZoneType;
	EQbCommandStates                = (BYTE*)g_eqCommandStates;
	EQMappableCommandList           = (char**)__BindList;
	ghEQMainInstance                = (HINSTANCE*)__heqmain;
	gpAutoFire                      = (BYTE*)__Autofire;
	gpAutoSkill                     = (AUTOSKILL*)__AutoSkillArray;
	gpbCommandEvent                 = (DWORD*)__gpbCommandEvent;
	gpbRangedAttackReady            = (char*)__RangeAttackReady;
	gpbShowNetStatus                = (char*)__NetStatusToggle;
	gpbUseTellWindows               = (bool*)__UseTellWindows;
	gpMouseEventTime                = (DWORD*)__MouseEventTime;
	gpPCNames                       = (DWORD*)__PCNames;
	gpShiftKeyDown                  = (BYTE*)__ShiftKeyDown; // addr+1=ctrl, addr+2=alt
	gpShowNames                     = (DWORD*)__ShowNames;
	pDynamicZone                    = (CDynamicZone*)instDynamicZone;
	pEQLogin                        = (EQLogin*)pinstEqLogin;
	pEQMisc                         = (EQMisc*)instEQMisc;
	pEQSuiteTextureLoader           = (CEQSuiteTextureLoader*)pinstEQSuiteTextureLoader;
	pEverQuestInfo                  = (EVERQUESTINFO*)pinstEverQuestInfo;
	pgCurrentSocial                 = (INT*)__CurrentSocial;
	pGuild                          = (CGuild*)__Guilds;
	pGuildList                      = (GUILDS*)__Guilds;
	pMouseLook                      = (char*)__MouseLook;
	pRaid                           = (PEQRAID)instCRaid;
	pScreenMode                     = (DWORD*)__ScreenMode;
	pScreenX                        = (DWORD*)__ScreenX;
	pScreenXMax                     = (DWORD*)__ScreenXMax;
	pScreenY                        = (DWORD*)__ScreenY;
	pScreenYMax                     = (DWORD*)__ScreenYMax;
	pSocialChangedList              = (EQSOCIALCHANGED*)__SocialChangedList;
	pSocialList                     = (EQSOCIAL*)__Socials;
	pSpellSets                      = (SpellLoadout*)pinstSpellSets;
	pTaskManager                    = (CTaskManager*)pinstCTaskManager;
	pTributeActive                  = (BYTE*)instTributeActive;
	pZoneInfo                       = (ZONEINFO*)instEQZoneInfo;

	// Spawn/Char pointers
#pragma warning(suppress: 4996)
	pCharData                       = pinstLocalPC;                    // deprecated
#pragma warning(suppress: 4996)
	pPCData                         = pinstLocalPC;                    // deprecated
	pLocalPC                        = pinstLocalPC;
	pActiveBanker                   = pinstActiveBanker;
	pActiveCorpse                   = pinstActiveCorpse;
	pActiveGMaster                  = pinstActiveGMaster;
	pActiveMerchant                 = pinstActiveMerchant;
#pragma warning(suppress: 4996)
	pCharSpawn                      = pinstControlledPlayer;           // deprecated
	pControlledPlayer               = pinstControlledPlayer;
	pLocalPlayer                    = pinstLocalPlayer;
	pTarget                         = pinstTarget;
	pTradeTarget                    = pinstTradeTarget;

	// Non-UI Foreign Pointers (pointer types in eq)
	pAltAdvManager                  = pinstAltAdvManager;
	pChatManager                    = pinstCChatWindowManager;
	pConnection                     = __gWorld;
	pContainerMgr                   = pinstCContainerMgr;
	pContextMenuManager             = pinstCContextMenuManager;
	pCurrentMapLabel                = __CurrentMapLabel;
	pDBStr                          = pinstCDBStr;
	pDisplay                        = pinstCDisplay;
	pDZMember                       = pinstDZMember;
	pDZTimerInfo                    = pinstDZTimerInfo;
	pEqSoundManager                 = pinstEQSoundManager;
	pEQSpellStrings                 = pinstEQSpellStrings;
	pEverQuest                      = pinstCEverQuest;
	pGraphicsEngine                 = pinstSGraphicsEngine;
	pInvSlotMgr                     = pinstCInvSlotMgr;
	pItemDisplayManager             = pinstCItemDisplayManager;
	pLootFiltersManager             = pinstLootFiltersManager;
#pragma warning(suppress: 4996)
	pMercInfo                       = pinstMercenaryData;              // deprecated
	pMercManager                    = pinstMercenaryData;
	pResolutionHandler              = pinstCResolutionHandler;
	pSidlMgr                        = pinstCSidlManager;
	pSkillMgr                       = pinstSkillMgr;
#pragma warning(suppress: 4996)
	pCSkillMgr                      = pinstSkillMgr;                   // deprecated
	pSpawnManager                   = pinstSpawnManager;
	pSpellMgr                       = pinstSpellManager;
	pStringTable                    = pinstStringTable;
	pSwitchMgr                      = pinstSwitchManager;
	pTaskMember                     = pinstTaskMember;
	pWndMgr                         = pinstCXWndManager;
	pWorldData                      = pinstWorldData;

	// UI Window Instance Pointers
	pAAWnd                          = pinstCAAWnd;
	pAchievementsWnd                = pinstCAchievementsWnd;
	pActionsWnd                     = pinstCActionsWnd;
	pAdvancedLootWnd                = pinstCAdvancedLootWnd;
	pAlarmWnd                       = pinstCAlarmWnd;
	pAuraWnd                        = pinstCAuraWnd;
	pBandolierWnd                   = pinstCBandolierWnd;
	pBankWnd                        = pinstCBankWnd;
	pBarterWnd                      = pinstCBarterWnd;
	pBarterSearchWnd                = pinstCBarterSearchWnd;
	pBazaarSearchWnd                = pinstCBazaarSearchWnd;
	pBazaarWnd                      = pinstCBazaarWnd;
	pBodyTintWnd                    = pinstCBodyTintWnd;
	pBookWnd                        = pinstCBookWnd;
	pBreathWnd                      = pinstCBreathWnd;
	pBuffWnd                        = pinstCBuffWindowNORMAL;
	pSongWnd                        = pinstCBuffWindowSHORT;
	pBugReportWnd                   = pinstCBugReportWnd;
	pCastingWnd                     = pinstCCastingWnd;
	pCastSpellWnd                   = pinstCCastSpellWnd;
	pCharacterListWnd               = pinstCCharacterListWnd;
	pColorPickerWnd                 = pinstCColorPickerWnd;
	pCombatAbilityWnd               = pinstCCombatAbilityWnd;
	pCombatSkillsSelectWnd          = pinstCCombatSkillsSelectWnd;
	pCompassWnd                     = pinstCCompassWnd;
	pConfirmationDialog             = pinstCConfirmationDialog;
	pCursorAttachment               = pinstCCursorAttachment;
	pEditLabelWnd                   = pinstCEditLabelWnd;
	pEQMainWnd                      = pinstCEQMainWnd;
	pExtendedTargetWnd              = pinstCExtendedTargetWnd;
	pFactionWnd                     = pinstCFactionWnd;
	pFileSelectionWnd               = pinstCFileSelectionWnd;
	pFindItemWnd                    = pinstCFindItemWnd;
	pFindLocationWnd                = pinstCFindLocationWnd;
	pFriendsWnd                     = pinstCFriendsWnd;
	pGemsGameWnd                    = pinstCGemsGameWnd;
	pGiveWnd                        = pinstCGiveWnd;
	pGroupSearchFiltersWnd          = pinstCGroupSearchFiltersWnd;
	pGroupSearchWnd                 = pinstCGroupSearchWnd;
	pGroupWnd                       = pinstCGroupWnd;
	pGuildMgmtWnd                   = pinstCGuildMgmtWnd;
	pHotButtonWnd                   = pinstCHotButtonWnd;
	pInspectWnd                     = pinstCInspectWnd;
	pInventoryWnd                   = pinstCInventoryWnd;
	pJournalCatWnd                  = pinstCJournalCatWnd;
	pJournalTextWnd                 = pinstCJournalTextWnd;
	pKeyRingWnd                     = pinstCKeyRingWnd;
	pLargeDialog                    = pinstCLargeDialogWnd;
	pLoadskinWnd                    = pinstCLoadskinWnd;
	pLootWnd                        = pinstCLootWnd;
	pMapToolbarWnd                  = pinstCMapToolbarWnd;
	pMapViewWnd                     = pinstCMapViewWnd;
	pMarketplaceWnd                 = pinstCMarketplaceWnd;
	pMerchantWnd                    = pinstCMerchantWnd;
	pMusicPlayerWnd                 = pinstCMusicPlayerWnd;
	pNoteWnd                        = pinstCNoteWnd;
	pOptionsWnd                     = pinstCOptionsWnd;
	pOverseerWnd                    = pinstCOverseerWnd;
	pPetInfoWnd                     = pinstCPetInfoWnd;
	pPetitionQWnd                   = pinstCPetitionQWnd;
	pPlayerCustomizationWnd         = pinstCPlayerCustomizationWnd;
	pPlayerNotesWnd                 = pinstCPlayerNotesWnd;
	pPlayerWnd                      = pinstCPlayerWnd;
	pPurchaseGroupWnd               = pinstCPurchaseGroupWnd;
	pQuantityWnd                    = pinstCQuantityWnd;
	pRaidOptionsWnd                 = pinstCRaidOptionsWnd;
	pRaidWnd                        = pinstCRaidWnd;
	pRealEstateItemsWnd             = pinstCRealEstateItemsWnd;
	pRespawnWnd                     = pinstCRespawnWnd;
	pRewardSelectionWnd             = pinstRewardSelectionWnd;
	pSelectorWnd                    = pinstCSelectorWnd;
	pSkillsSelectWnd                = pinstCSkillsSelectWnd;
	pSkillsWnd                      = pinstCSkillsWnd;
	pSocialEditWnd                  = pinstCSocialEditWnd;
	pSpellBookWnd                   = pinstCSpellBookWnd;
	pStoryWnd                       = pinstCStoryWnd;
	pTargetWnd                      = pinstCTargetWnd;
	pTaskWnd                        = pinstCTaskWnd;
	pTextEntryWnd                   = pinstCTextEntryWnd;
	pTimeLeftWnd                    = pinstCTimeLeftWnd;
	pTrackingWnd                    = pinstCTrackingWnd;
	pTradeWnd                       = pinstCTradeWnd;
	pTrainWnd                       = pinstCTrainWnd;
	pVideoModesWnd                  = pinstCVideoModesWnd;
	pZoneGuideWnd                   = pinstCZoneGuideWnd;

	g_pDrawHandler                  = pinstRenderInterface;

	NewUIINI                        = (fEQNewUIINI)__NewUIINI;
	ProcessGameEvents               = (fEQProcGameEvts)__ProcessGameEvents;
	GetLabelFromEQ                  = (fGetLabelFromEQ)__GetLabelFromEQ;
}

#pragma endregion

#pragma region EQGraphicsDX9.dll offsets

//============================================================================
//
// EQGraphicsDX9.dll Offsets
INITIALIZE_EQGRAPHICS_OFFSET(__eqgraphics_fopen);
INITIALIZE_EQGRAPHICS_OFFSET(CParticleSystem__Render);
INITIALIZE_EQGRAPHICS_OFFSET(CRender__RenderScene);
INITIALIZE_EQGRAPHICS_OFFSET(CRender__UpdateDisplay);
INITIALIZE_EQGRAPHICS_OFFSET(CRender__ResetDevice);

void InitializeEQGraphicsOffsets()
{
	if (!EQGraphicsBaseAddress)
	{
		// no EQGraphicsDx9.dll loaded yet
		HMODULE hLibrary = LoadLibrary("EQGraphicsDX9.dll");
		EQGraphicsBaseAddress = (uintptr_t)hLibrary;

		__eqgraphics_fopen = FixEQGraphicsOffset(__eqgraphics_fopen_x);
		CParticleSystem__Render = FixEQGraphicsOffset(CParticleSystem__Render_x);
		CRender__RenderScene = FixEQGraphicsOffset(CRender__RenderScene_x);
		CRender__UpdateDisplay = FixEQGraphicsOffset(CRender__UpdateDisplay_x);
		CRender__ResetDevice = FixEQGraphicsOffset(CRender__ResetDevice_x);
	}
}

#pragma endregion

#pragma region eqmain.dll offsets

//============================================================================
//
// EQMain.dll

//----------------------------------------------------------------------------
// offsets / patterns

DWORD EQMain__CEQSuiteTextureLoader__GetTexture = 0;
DWORD EQMain__CLoginViewManager__HandleLButtonUp = 0;
DWORD EQMain__CXWndManager__GetCursorToDisplay = 0;
DWORD EQMain__LoginController__GiveTime = 0;
DWORD EQMain__LoginController__ProcessKeyboardEvents = 0;
DWORD EQMain__LoginController__ProcessMouseEvents = 0;
DWORD EQMain__LoginController__Shutdown = 0;
DWORD EQMain__LoginServerAPI__JoinServer = 0;
DWORD EQMain__WndProc = 0;

DWORD EQMain__pinstCEQSuiteTextureLoader = 0;
DWORD EQMain__pinstCLoginViewManager = 0;
DWORD EQMain__pinstCSidlManager = 0;
DWORD EQMain__pinstCXWndManager = 0;
DWORD EQMain__pinstLoginController = 0;
DWORD EQMain__pinstLoginServerAPI = 0;
DWORD EQMain__pinstServerInfo = 0;

ForeignPointer<CLoginViewManager> g_pLoginViewManager;
ForeignPointer<LoginClient> g_pLoginClient;
ForeignPointer<LoginController> g_pLoginController;
ForeignPointer<LoginServerAPI> g_pLoginServerAPI;

FUNCTION_AT_VARIABLE_ADDRESS(int CLoginViewManager::HandleLButtonUp(CXPoint&), EQMain__CLoginViewManager__HandleLButtonUp);
FUNCTION_AT_VARIABLE_ADDRESS(unsigned int LoginServerAPI::JoinServer(int, void*, int), EQMain__LoginServerAPI__JoinServer);

// We use CLoginViewManager to find the ServerInfo instance

bool InitializeEQMainOffsets()
{
	if (*ghEQMainInstance)
	{
		EQMainBaseAddress = (uintptr_t)*ghEQMainInstance;

		EQMain__CEQSuiteTextureLoader__GetTexture = FixEQMainOffset(EQMain__CEQSuiteTextureLoader__GetTexture_x);
		EQMain__CLoginViewManager__HandleLButtonUp = FixEQMainOffset(EQMain__CLoginViewManager__HandleLButtonUp_x);
		EQMain__CXWndManager__GetCursorToDisplay = FixEQMainOffset(EQMain__CXWndManager__GetCursorToDisplay_x);
		EQMain__LoginController__GiveTime = FixEQMainOffset(EQMain__LoginController__GiveTime_x);
		EQMain__LoginServerAPI__JoinServer = FixEQMainOffset(EQMain__LoginServerAPI__JoinServer_x);
		EQMain__LoginController__Shutdown = FixEQMainOffset(EQMain__LoginController__Shutdown_x);
		EQMain__WndProc = FixEQMainOffset(EQMain__WndProc_x);

		EQMain__pinstCEQSuiteTextureLoader = FixEQMainOffset(EQMain__pinstCEQSuiteTextureLoader_x);
		EQMain__pinstCLoginViewManager = FixEQMainOffset(EQMain__pinstCLoginViewManager_x);
		EQMain__pinstCSidlManager = FixEQMainOffset(EQMain__pinstCSidlManager_x);
		EQMain__pinstCXWndManager = FixEQMainOffset(EQMain__pinstCXWndManager_x);
		EQMain__pinstLoginController = FixEQMainOffset(EQMain__pinstLoginController_x);
		EQMain__pinstLoginServerAPI = FixEQMainOffset(EQMain__pinstLoginServerAPI_x);
		EQMain__pinstServerInfo = EQMain__pinstCLoginViewManager - 4;

		if (EQMain__LoginController__GiveTime)
		{
			//.text:10014B00                      public: void __thiscall LoginController::GiveTime(void) proc near
			//.text:10014B00 56                                   push    esi
			//.text:10014B01 8B F1                                mov     esi, this
			//.text:10014B03 E8 D8 06 00 00                       call    LoginController::ProcessKeyboardEvents(void)
			EQMain__LoginController__ProcessKeyboardEvents = GetFunctionAddressAt(EQMain__LoginController__GiveTime + 3, 1, 4);
			//.text:10014B08 8B CE                                mov     this, esi       ; this
			//.text:10014B0A 5E                                   pop     esi
			//.text:10014B0B E9 A0 08 00 00                       jmp     LoginController::ProcessMouseEvents(void)
			EQMain__LoginController__ProcessMouseEvents = GetFunctionAddressAt(EQMain__LoginController__GiveTime + 11, 1, 4);
		}

		g_pLoginViewManager = EQMain__pinstCLoginViewManager;
		g_pLoginController = EQMain__pinstLoginController;
		g_pLoginServerAPI = EQMain__pinstLoginServerAPI;
		g_pLoginClient = EQMain__pinstServerInfo;

		// Update addresses shared with eqgame.exe
		CEQSuiteTextureLoader__GetTexture = EQMain__CEQSuiteTextureLoader__GetTexture;
		pEQSuiteTextureLoader = (CEQSuiteTextureLoader*)EQMain__pinstCEQSuiteTextureLoader;

		return true;
	}

	return false;
}

void CleanupEQMainOffsets()
{
	EQMainBaseAddress = 0;

	EQMain__CEQSuiteTextureLoader__GetTexture = 0;
	EQMain__CLoginViewManager__HandleLButtonUp = 0;
	EQMain__CXWndManager__GetCursorToDisplay = 0;
	EQMain__LoginController__GiveTime = 0;
	EQMain__LoginController__ProcessKeyboardEvents = 0;
	EQMain__LoginController__ProcessMouseEvents = 0;
	EQMain__LoginController__Shutdown = 0;
	EQMain__LoginServerAPI__JoinServer = 0;
	EQMain__WndProc = 0;

	EQMain__pinstCEQSuiteTextureLoader = 0;
	EQMain__pinstCLoginViewManager = 0;
	EQMain__pinstCSidlManager = 0;
	EQMain__pinstCXWndManager = 0;
	EQMain__pinstLoginController = 0;
	EQMain__pinstLoginServerAPI = 0;
	EQMain__pinstServerInfo = 0;

	g_pLoginController.reset();
	g_pLoginViewManager.reset();
	g_pLoginServerAPI.reset();
	g_pLoginClient.reset();

	// re-initialize offsets that were overwritten by eqmain
	pEQSuiteTextureLoader = (CEQSuiteTextureLoader*)pinstEQSuiteTextureLoader;
	CEQSuiteTextureLoader__GetTexture = FixEQGameOffset(CEQSuiteTextureLoader__GetTexture_x);
}

#pragma endregion

//============================================================================
//

void InitializeGlobals()
{
	ZeroMemory(gDiKeyName, sizeof(gDiKeyName));
	for (int i = 0; gDiKeyID[i].Id; i++)
	{
		gDiKeyName[gDiKeyID[i].Id] = gDiKeyID[i].szName;
	}

	InitializeGlobalOffsets();
	InitializeEQGameOffsets();
	InitializeEQGraphicsOffsets();
}

} // namespace eqlib

#if __has_include("globals-private.cpp")
#include "globals-private.cpp"
#endif
